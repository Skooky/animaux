package com.findmehome.findmehome.data.contract;

import android.provider.BaseColumns;

import com.findmehome.findmehome.data.contract.base.ContractBase;

/**
 * Created by maxime on 13/06/2017.
 */

public class AddressContract extends ContractBase {
    /**
     * Name of the table.
     */
    public static final String TABLE_NAME = "Address";

    /**
     * Name of fields DB.
     */
    public static final String FIELD_STREET = "street";
    public static final String FIELD_CITY = "city";
    public static final String FIELD_ZIPCODE = "zipcode";

    /**
     * Columns.
     */
    public static final String[] COLUMNS = {
            AddressContract.TABLE_NAME + "." + AddressContract.FIELD_ID + " AS " + BaseColumns._ID,
            AddressContract.TABLE_NAME + "." + AddressContract.FIELD_SERVERID,
            AddressContract.TABLE_NAME + "." + AddressContract.FIELD_STREET,
            AddressContract.TABLE_NAME + "." + AddressContract.FIELD_CITY,
            AddressContract.TABLE_NAME + "." + AddressContract.FIELD_ZIPCODE,
    };

    @Override
    public String createTable() {
        return "CREATE TABLE " + AddressContract.TABLE_NAME + " ( " +
                AddressContract.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                AddressContract.FIELD_SERVERID + " INTEGER, " +
                AddressContract.FIELD_CREATED_DATE + " DATETIME, " +
                AddressContract.FIELD_UPDATED_DATE + " DATETIME, " +
                AddressContract.FIELD_SYNC_DATE + " DATETIME, " +
                AddressContract.FIELD_STREET + " TEXT NOT NULL, " +
                AddressContract.FIELD_CITY + " TEXT NOT NULL, " +
                AddressContract.FIELD_ZIPCODE + " TEXT NOT NULL " +
                ");";
    }
}
