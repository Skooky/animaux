package com.findmehome.findmehome.data.webservice.base;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Base64;
import android.util.LruCache;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.findmehome.findmehome.FindMeHomeApplication;
import com.findmehome.findmehome.utils.PreferencesUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sandra on 13/06/17.
 */

public class RestClientBase {

    /** Context. */
    private static Context context;
    /** Request queue. */
    private RequestQueue requestQueue;
    /** ImageLoader. */
    private ImageLoader imageLoader;
    /** Rest client base. */
    private static RestClientBase instance;

    /** Url of the api. */
    private static final String URL = "http://10.3.12.34:8765/";

    /**
     * Rest client base constructor.
     * @param ctx {@link Context}
     */
    public RestClientBase(Context ctx) {
        context = ctx;
        this.requestQueue = this.getRequestQueue();

        this.imageLoader = new ImageLoader(this.requestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    /**
     * Get instance of {@link RestClientBase}
     * @param context {@link Context}
     * @return RestClientBase
     */
    public static synchronized RestClientBase getInstance(Context context) {
        if (instance == null) {
            instance = new RestClientBase(context);
        }

        return instance;
    }

    /**
     * Get the requestQueue.
     * @return RequestQueue
     */
    public RequestQueue getRequestQueue() {
        if (this.requestQueue == null) {
            if (context == null) {
                context = FindMeHomeApplication.context;
            }
            this.requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return this.requestQueue;
    }

    /**
     * Add to request queue.
     * @param request {@link Request}
     * @param <T> Entity
     */
    public <T> void addToRequestQueue(Request<T> request) {
        this.getRequestQueue().add(request);
    }

    /**
     * Get the image loader.
     * @return ImageLoader
     */
    public ImageLoader getImageLoader() {
        return this.imageLoader;
    }

    /**
     * Get the json request.
     * @param url {@link String}
     * @return RequestFuture<JSONObject>
     */
    public RequestFuture<JSONObject> getJSONRequest(String url) {
        RequestFuture<JSONObject> futureRequest = RequestFuture.newFuture();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, URL+url, null, futureRequest, futureRequest) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeader();
            }
        };

        RestClientBase.getInstance(context).addToRequestQueue(jsonObjectRequest);

        return futureRequest;
    }

    /**
     * Get JSONArrayRequest.
     * @param url String
     * @return RequestFuture<JSONArray>
     */
    public RequestFuture<JSONArray> getJSONArrayRequest(String url) {

        RequestFuture<JSONArray> futureRequest = RequestFuture.newFuture();

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.GET, URL+url, null, futureRequest, futureRequest) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeader();
            }
        };

        RestClientBase.getInstance(context).addToRequestQueue(jsonObjectRequest);

        return futureRequest;
    }

    /**
     * POST JSONRequest
     * @param url Url of the api
     * @param postObject {@link JSONObject}
     * @return RequestFuture<JSONObject>
     */
    public RequestFuture<JSONObject> postJSONRequest(String url, JSONObject postObject) {
        RequestFuture<JSONObject> futureRequest = RequestFuture.newFuture();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST, URL+url, postObject, futureRequest, futureRequest) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeader();
            }
        };

        RestClientBase.getInstance(context).addToRequestQueue(jsonObjectRequest);

        return futureRequest;
    }

    /**
     * POST JSONArray request.
     * @param url {@link String}
     * @param postArray JSONArray
     * @return RequestFuture<JSONArray>
     */
    public RequestFuture<JSONArray> postJSONArrayRequest(String url, JSONArray postArray) {

        RequestFuture<JSONArray> futureRequest = RequestFuture.newFuture();

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.POST, URL+url, postArray, futureRequest, futureRequest) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeader();
            }
        };

        RestClientBase.getInstance(context).addToRequestQueue(jsonObjectRequest);

        return futureRequest;
    }

    /**
     * PUT JsonRequest.
     * @param url Url of the api
     * @param putObject {@link JSONObject}
     * @return RequestFuture<JSONObject>
     */
    public RequestFuture<JSONObject> putJSONRequest(String url, JSONObject putObject) {
        RequestFuture<JSONObject> futureRequest = RequestFuture.newFuture();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.PUT, URL+url, putObject, futureRequest, futureRequest) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeader();
            }
        };

        RestClientBase.getInstance(context).addToRequestQueue(jsonObjectRequest);

        return futureRequest;
    }

    /**
     * PUT JSONArrayRequest.
     * @param url Url of the api
     * @param putArray {@link JSONArray}
     * @return RequestFuture<JSONArray>
     */
    public RequestFuture<JSONArray> putJSONArrayRequest(String url, JSONArray putArray) {

        RequestFuture<JSONArray> futureRequest = RequestFuture.newFuture();

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.PUT, URL+url, putArray, futureRequest, futureRequest) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeader();
            }
        };

        RestClientBase.getInstance(context).addToRequestQueue(jsonObjectRequest);

        return futureRequest;
    }

    /**
     * DELETE JSONRequest.
     * @param url {@link String}
     * @return RequestFuture<JSONObject>
     */
    public RequestFuture<JSONObject> deleteJSONRequest(String url) {
        RequestFuture<JSONObject> futureRequest = RequestFuture.newFuture();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.DELETE, URL+url, null, futureRequest, futureRequest) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeader();
            }
        };

        RestClientBase.getInstance(context).addToRequestQueue(jsonObjectRequest);

        return futureRequest;
    }

    /**
     * Delete JSONArrayRequest.
     * @param url {@link String}
     * @param deleteArray {@link JSONArray}
     * @return RequestFuture<JSONArray>
     */
    public RequestFuture<JSONArray> deleteJSONArrayRequest(String url, JSONArray deleteArray) {

        RequestFuture<JSONArray> futureRequest = RequestFuture.newFuture();

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                Request.Method.DELETE, URL+url, deleteArray, futureRequest, futureRequest) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return getHeader();
            }
        };

        RestClientBase.getInstance(context).addToRequestQueue(jsonObjectRequest);

        return futureRequest;
    }

    /**
     * Get the header of the request.
     * @return Map<String, String>
     */
    private Map<String, String> getHeader() {
        Map<String, String> headers = new HashMap<>();

        if (!TextUtils.isEmpty(PreferencesUtils.getToken(this.context))) {
            String auth = "Bearer " + PreferencesUtils.getToken(this.context);

            headers.put("Authorization", auth);
        }

        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json");

        return headers;
    }
}
