package com.findmehome.findmehome.data.base.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.findmehome.findmehome.data.base.adapter.base.SQLiteAdapterBase;
import com.findmehome.findmehome.data.contract.AdContract;
import com.findmehome.findmehome.entity.Ad;
import com.findmehome.findmehome.utils.DateUtils;
import com.findmehome.findmehome.utils.PreferencesUtils;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by maxime on 13/06/2017.
 */

public class AdSQLiteAdapter extends SQLiteAdapterBase<Ad> {

    /**
     * Constructor.
     *
     * @param context Le context
     */
    public AdSQLiteAdapter(Context context) {
        super(context);
    }

    @Override
    public long insert(Ad model) {
        this.open();

        long result = this.db.insert(AdContract.TABLE_NAME, null, this.itemToContentValues(model));

        this.close();

        return result;
    }

    @Override
    public void update(Ad model) {
        this.open();

        model.setUpdatedDate(new DateTime());

        this.db.update(
                AdContract.TABLE_NAME,
                this.itemToContentValues(model),
                AdContract.FIELD_ID + " = ?",
                new String[] {String.valueOf(model.getId())});

        this.close();
    }

    @Override
    public long insertOrUpdate(Ad model) {
        Ad dbAd = this.getWithServerId(model.getServerId());
        long result = -1;

        if (dbAd != null) {
            model.setId(dbAd.getId());
            model.setSyncDate(PreferencesUtils.getSyncDate(this.getContext()));

            this.update(model);
        } else {
            result = this.insert(model);
        }

        return result;
    }

    @Override
    public void insertOrUpdate(ArrayList<Ad> models) {
        for (Ad Ad : models) {
            this.insertOrUpdate(Ad);
        }
    }

    @Override
    public void delete(Ad model) {
        this.deleteWithId(model.getId());
    }

    @Override
    public void deleteWithId(int modelId) {
        this.open();

        this.db.delete(AdContract.TABLE_NAME, AdContract.FIELD_ID + " = ?", new String[] {String.valueOf(modelId)});

        this.close();
    }

    @Override
    public Ad get(Ad model) {
        return this.getWithId(model.getId());
    }

    @Override
    public Ad getWithId(int modelId) {
        this.open();
        Ad result = null;

        Cursor cursor = this.query(
                AdContract.FIELD_ID + " = ?",
                new String[] {String.valueOf(modelId)},
                null,
                null,
                null,
                null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            result = this.cursorToItem(cursor);

            cursor.close();
        }

        this.close();

        return result;
    }

    @Override
    public Ad getWithServerId(int modelServerId) {
        this.open();
        Ad result = null;

        Cursor cursor = this.query(
                AdContract.FIELD_SERVERID + " = ?",
                new String[] {String.valueOf(modelServerId)},
                null,
                null,
                null,
                null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            result = this.cursorToItem(cursor);

            cursor.close();
        }

        this.close();

        return result;
    }

    @Override
    public ArrayList<Ad> getAll() {
        this.open();

        ArrayList<Ad> result;

        Cursor cursor = this.query(null, null, null, null, null, null);

        result = this.cursorToItems(cursor);

        this.close();
        return result;
    }

    /**
     * Get all Ads.
     * @return Cursor a cursor
     */
    public Cursor getAllCursor() {
        this.open();

        Cursor cursor = this.query(null, null, null, null, null, null);

        //this.close();

        return cursor;
    }

    @Override
    public ContentValues itemToContentValues(Ad item) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(AdContract.FIELD_SERVERID, item.getServerId());

        contentValues.put(AdContract.FIELD_TITLE, item.getTitle());
        contentValues.put(AdContract.FIELD_CONTENT, item.getContent());
        contentValues.put(AdContract.FIELD_ISVALIDATED, item.isValidated());

        if (item.getPublished() != null) {
            contentValues.put(AdContract.FIELD_PUBLISHED,
                    DateUtils.formatDateTimeToString(item.getPublished()));
        }

        if (item.getUser() != null) {
            contentValues.put(AdContract.FIELD_USER, item.getUser().getId());
        }

        if (item.getAnimal() != null) {
            contentValues.put(AdContract.FIELD_ANIMAL, item.getAnimal().getId());
        }

        if (item.getCreatedDate() == null) {
            item.setCreatedDate(new DateTime());
        }

        contentValues.put(AdContract.FIELD_CREATED_DATE,
                DateUtils.formatDateTimeToString(item.getCreatedDate()));

        if (item.getUpdatedDate() != null) {
            contentValues.put(AdContract.FIELD_UPDATED_DATE,
                    DateUtils.formatDateTimeToString(item.getUpdatedDate()));
        }

        if (item.getSyncDate() != null) {
            contentValues.put(AdContract.FIELD_SYNC_DATE,
                    DateUtils.formatDateTimeToString(item.getSyncDate()));
        }

        return contentValues;
    }

    @Override
    public Ad cursorToItem(Cursor cursor) {
        Ad result = new Ad();
        int column;

        if ((column = cursor.getColumnIndex(BaseColumns._ID)) > -1) {
            result.setId(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_SERVERID)) > -1) {
            result.setServerId(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_TITLE)) > -1) {
            result.setTitle(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_CONTENT)) > -1) {
            result.setContent(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_PUBLISHED)) > -1) {
            if (cursor.getString(column) != null) {
                result.setPublished(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_ISVALIDATED)) > -1) {
            result.setValidated(cursor.getInt(column) == 1);
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_USER)) > -1) {
            result.setUser(new UserSQLiteAdapter(this.context).getWithId(cursor.getInt(column)));
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_ANIMAL)) > -1) {
            result.setAnimal(new AnimalSQLiteAdapter(this.context).getWithId(cursor.getInt(column)));
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_CREATED_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setCreatedDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_UPDATED_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setUpdatedDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        if ((column = cursor.getColumnIndex(AdContract.FIELD_SYNC_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setSyncDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        return result;
    }

    @Override
    public ArrayList<Ad> cursorToItems(Cursor cursor) {
        ArrayList<Ad> result = new ArrayList<>();

        if (cursor.getCount() != 0) {
            while (cursor.moveToNext()) {
                result.add(this.cursorToItem(cursor));
            }

            cursor.close();
        }

        return result;
    }

    @Override
    public Cursor query(
            String whereClause,
            String[] whereArgs,
            String groupBy,
            String having,
            String orderBy,
            String limit) {
        return this.db.query(
                AdContract.TABLE_NAME,
                AdContract.COLUMNS,
                whereClause,
                whereArgs,
                groupBy,
                having,
                orderBy,
                limit);
    }

    @Override
    public void deleteUnused() {
        this.db.delete(
                AdContract.TABLE_NAME,
                AdContract.FIELD_SYNC_DATE + " <> ?",
                new String[] { String.valueOf(PreferencesUtils.getSyncDate(this.getContext()).getMillis())}
        );
    }
}
