package com.findmehome.findmehome.data.contract.base;

/**
 * Created by maxime on 13/06/2017.
 */

public abstract class ContractBase<T> {
    /** Name of the table DB. */
    public static final String TABLE_NAME = "";
    /** Id field DB. */
    public static final String FIELD_ID = "id";
    /** Server Id field DB. */
    public static final String FIELD_SERVERID = "serverId";
    /** CreateDate field DB. */
    public static final String FIELD_CREATED_DATE = "createdDate";
    /** UpdateDate field DB. */
    public static final String FIELD_UPDATED_DATE = "updatedDate";
    /** SyncDate field DB. */
    public static final String FIELD_SYNC_DATE = "syncDate";

    /** All columns of the entity. */
    public static final String[] COLUMNS = {};

    /**
     * Get the creation script for the table.
     * @return the script
     */
    public abstract String createTable();
}
