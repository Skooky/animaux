package com.findmehome.findmehome.entity;

/**
 * Created by sandra on 25/06/17.
 */

public enum Race {
    DOG("Chien", 1),
    CAT("Chat", 2);

    private String stringValue;
    private int intValue;

    private Race(String toString, int value) {
        stringValue = toString;
        intValue = value;
    }

    public int getIntValue() {
        return intValue;
    }

    @Override
    public String toString() {
        return stringValue;
    }
}
