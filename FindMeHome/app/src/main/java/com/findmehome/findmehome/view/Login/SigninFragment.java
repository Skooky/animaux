package com.findmehome.findmehome.view.Login;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.findmehome.findmehome.R;
import com.findmehome.findmehome.data.webservice.AddressWebServiceAdapter;
import com.findmehome.findmehome.data.webservice.UserWebServiceAdapter;
import com.findmehome.findmehome.entity.Address;
import com.findmehome.findmehome.entity.User;

/**
 * A placeholder fragment containing a simple view.
 */
public class SigninFragment extends Fragment implements View.OnClickListener {

    protected TextView lastnameView;
    protected EditText lastnameText;
    protected TextView firstnameView;
    protected EditText firstnameText;
    protected TextView usernameView;
    protected EditText usernameText;
    protected TextView emailView;
    protected EditText emailText;
    protected TextView passwordView;
    protected EditText passwordText;
    protected TextView phoneView;
    protected EditText phoneText;
    protected TextView addressView;
    protected EditText addressText;
    protected TextView postalcodeView;
    protected EditText postalcodeText;
    protected TextView cityView;
    protected EditText cityText;
    protected TextView sirenView;
    protected EditText sirenText;
    protected TextView societyView;
    protected EditText societyText;
    protected TextView isProView;
    protected CheckBox isProCheckBox;
    protected Button signinButton;

    private User user;
    private Address address;

    public SigninFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signin, container, false);

        this.initializeView(view);

        return view;
    }

    /**
     * Initialize view.
     * @param view View
     */
    private void initializeView(View view) {
        this.lastnameView = (TextView) view.findViewById(R.id.lastname_label);
        this.lastnameText = (EditText) view.findViewById(R.id.lastname_text);
        this.firstnameView = (TextView) view.findViewById(R.id.firstname_label);
        this.firstnameText = (EditText) view.findViewById(R.id.firstname_text);
        this.usernameView = (TextView) view.findViewById(R.id.username_label);
        this.usernameText = (EditText) view.findViewById(R.id.username_text);
        this.emailView = (TextView) view.findViewById(R.id.email_label);
        this.emailText = (EditText) view.findViewById(R.id.email_text);
        this.passwordView = (TextView) view.findViewById(R.id.password_label);
        this.passwordText = (EditText) view.findViewById(R.id.password_text);
        this.phoneView = (TextView) view.findViewById(R.id.phone_label);
        this.phoneText = (EditText) view.findViewById(R.id.phone_text);
        this.addressView = (TextView) view.findViewById(R.id.address_label);
        this.addressText = (EditText) view.findViewById(R.id.address_text);
        this.postalcodeView = (TextView) view.findViewById(R.id.postalcode_label);
        this.postalcodeText = (EditText) view.findViewById(R.id.postalcode_text);
        this.cityView = (TextView) view.findViewById(R.id.city_label);
        this.cityText = (EditText) view.findViewById(R.id.city_text);
        this.sirenView = (TextView) view.findViewById(R.id.siren_label);
        this.sirenText = (EditText) view.findViewById(R.id.siren_text);
        this.societyView = (TextView) view.findViewById(R.id.society_label);
        this.societyText = (EditText) view.findViewById(R.id.society_text);
        this.isProView = (TextView) view.findViewById(R.id.ispro_label);
        this.isProCheckBox = (CheckBox) view.findViewById(R.id.ispro_checkbox);
        this.signinButton = (Button) view.findViewById(R.id.signin_button);

        this.signinButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        final String password = SigninFragment.this.passwordText.getText().toString();

        new AsyncTask<Void, Boolean, Boolean>() {
            ProgressDialog progressDialog = new ProgressDialog(SigninFragment.this.getActivity());

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                progressDialog.setTitle("Inscription...");
                progressDialog.setMessage("Inscription en cours ...");
                progressDialog.show();
            }

            @Override
            protected Boolean doInBackground(Void... params) {
                boolean result = false;

                if(SigninFragment.this.validateData()) {
                    result = true;

                    new UserWebServiceAdapter(SigninFragment.this.getActivity()).post(user, password);
                }


                return result;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);

                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                if (aBoolean) {
                    SigninFragment.this.getActivity().finish();
                } else {
                    Toast.makeText(
                            SigninFragment.this.getActivity(),
                            "Une erreur s'est produite",
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }

        }.execute();
    }

    /**
     * Check if the fields isn't empty.
     * @return true if all fill
     */
    private boolean validateData() {
        boolean result = true;
        int message = 0;

        if (TextUtils.isEmpty(this.emailText.getText().toString())
                || !Patterns.EMAIL_ADDRESS.matcher(this.emailText.getText().toString()).matches()) {
            message = R.string.error_invalid_email;
        }

        if (TextUtils.isEmpty(this.firstnameText.getText().toString())) {
            message = R.string.error_invalid_firstname;
        }

        if (TextUtils.isEmpty(this.usernameText.getText().toString())) {
            message = R.string.error_invalid_username;
        }

        if (TextUtils.isEmpty(this.lastnameText.getText().toString())) {
            message = R.string.error_invalid_lastname;
        }

        if (TextUtils.isEmpty(this.passwordText.getText().toString())) {
            message = R.string.error_invalid_password;
        }

        if (message != 0) {
            result = false;

            Toast.makeText(this.getActivity(), this.getString(message), Toast.LENGTH_SHORT).show();
        } else {
            this.user = new User();
            this.user.setPro(this.isProCheckBox.isChecked());
            this.user.setSiren(this.sirenText.getText().toString());
            this.user.setUserName(this.usernameText.getText().toString());
            this.user.setFirstName(this.firstnameText.getText().toString());
            this.user.setLastName(this.lastnameText.getText().toString());
            this.user.setEmail(this.emailText.getText().toString());
            this.user.setPhone(this.phoneText.getText().toString());
            this.user.setSocietyName(this.societyText.getText().toString());

            this.address = new Address();
            this.address.setStreet(this.addressText.getText().toString());
            this.address.setZipcode(this.postalcodeText.getText().toString());
            this.address.setCity(this.cityText.getText().toString());

            this.user.setAddress(this.address);
        }

        return result;
    }
}
