package com.findmehome.findmehome.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.findmehome.findmehome.entity.base.EntityBase;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by maxime on 13/06/2017.
 */

public class Ad extends EntityBase implements Serializable, Parcelable {
    /** Title. */
    private String title;
    /** Content. */
    private String content;
    /** Published. */
    private DateTime published;
    /** Is validated. */
    private boolean isValidated;
    /** {@link User}. */
    private User user;
    /** {@link Animal}. */
    private Animal animal;

    /**
     * Constructor.
     */
    public Ad() {

    }

    /**
     * Get title.
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set title.
     * @param title String
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Get content.
     * @return String
     */
    public String getContent() {
        return content;
    }

    /**
     * Set content.
     * @param content String
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * Get publisher.
     * @return DateTime
     */
    public DateTime getPublished() {
        return published;
    }

    /**
     * Set published.
     * @param published DateTime
     */
    public void setPublished(DateTime published) {
        this.published = published;
    }

    /**
     * Get validated.
     * @return boolean
     */
    public boolean isValidated() {
        return isValidated;
    }

    /**
     * Set validated.
     * @param validated boolean
     */
    public void setValidated(boolean validated) {
        isValidated = validated;
    }

    /**
     * Get user.
     * @return User
     */
    public User getUser() {
        return user;
    }

    /**
     * Set user.
     * @param user {@link User}
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get animal.
     * @return Animal
     */
    public Animal getAnimal() {
        return animal;
    }

    /**
     * Set animal.
     * @param animal {@link Animal}
     */
    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    /**
     * Constructor.
     * @param in {@link Parcel}
     */
    protected Ad(Parcel in) {
        super(in);

        this.title = in.readString();
        this.content = in.readString();
        this.published = new DateTime(in.readLong());
        this.isValidated = in.readByte() != 0;
        this.user = in.readParcelable(User.class.getClassLoader());
        this.animal = in.readParcelable(Animal.class.getClassLoader());
    }

    /**
     * PARCEL creator.
     */
    public static final Creator<Ad> CREATOR = new Creator<Ad>() {
        @Override
        public Ad createFromParcel(Parcel in) {
            return new Ad(in);
        }

        @Override
        public Ad[] newArray(int size) {
            return new Ad[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeString(title);
        parcel.writeString(content);

        if (this.published != null) {
            parcel.writeLong(this.published.getMillis());
        }

        parcel.writeByte((byte) (isValidated ? 1 : 0));
        parcel.writeParcelable(user, i);
        parcel.writeParcelable(animal, i);
    }

}
