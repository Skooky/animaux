package com.findmehome.findmehome.data.webservice;

import android.content.Context;
import android.util.Log;

import com.findmehome.findmehome.data.base.adapter.AdSQLiteAdapter;
import com.findmehome.findmehome.data.contract.AdContract;
import com.findmehome.findmehome.data.webservice.base.RestClientBase;
import com.findmehome.findmehome.data.webservice.base.WebServiceAdapterBase;
import com.findmehome.findmehome.entity.Ad;
import com.findmehome.findmehome.utils.DateUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by sandra on 13/06/17.
 */

public class AdWebServiceAdapter extends WebServiceAdapterBase<Ad> {

    /** Address for the entity. */
    public static final String ADS = "ads";
    /** {@link UserWebServiceAdapter}. */
    private UserWebServiceAdapter userWebServiceAdapter;
    /** {@link AnimalWebServiceAdapter}. */
    private AnimalWebServiceAdapter animalWebServiceAdapter;

    private AdSQLiteAdapter adSQLiteAdapter;

    /**
     * {@link AdWebServiceAdapter} Constructor.
     * @param context {@link Context}
     */
    public AdWebServiceAdapter(Context context) {
        super(context);
        userWebServiceAdapter = new UserWebServiceAdapter(context);
        animalWebServiceAdapter = new AnimalWebServiceAdapter(context);
        adSQLiteAdapter = new AdSQLiteAdapter(context);
    }

    @Override
    public Ad jsonToObject(JSONObject jsonObject, boolean isData) {

        Ad result = new Ad();

        try {
            JSONObject object = jsonObject;

            if (isData) {
                object = (JSONObject) jsonObject.get("data");
            }
            result.setServerId(object.getInt("id"));
            result.setTitle(object.getString(AdContract.FIELD_TITLE));
            result.setContent(object.getString(AdContract.FIELD_CONTENT));
            result.setPublished(DateUtils.formatStringToDateTime(object.getString(AdContract.FIELD_PUBLISHED)));
            result.setValidated(object.getBoolean(AdContract.FIELD_ISVALIDATED));

            //JSONObject user = (JSONObject) object.get("user");
            //result.setUser(userWebServiceAdapter.jsonToObject(user, false));

            JSONObject animal = (JSONObject) object.get("animal");
            result.setAnimal(animalWebServiceAdapter.jsonToObject(animal, false));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public ArrayList<Ad> jsonArrayToObject(JSONArray jsonArray) {
        ArrayList<Ad> object = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                object.add(this.jsonToObject(jsonArray.getJSONObject(i), false));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return object;
    }

    @Override
    public JSONObject objectToJson(Ad object) {
        JSONObject result = new JSONObject();
        try {
            result.put(AdContract.FIELD_TITLE, object.getTitle());
            result.put(AdContract.FIELD_CONTENT, object.getContent());
            result.put(AdContract.FIELD_PUBLISHED, DateUtils.formatDateTimeToString(object.getPublished()));
            result.put(AdContract.FIELD_ISVALIDATED, object.isValidated());
            result.put(AdContract.FIELD_USER, object.getUser().getId());
            result.put(AdContract.FIELD_ANIMAL, object.getAnimal().getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public JSONArray objectToJsonArray(ArrayList<Ad> objects) {
        JSONArray jsonArray = new JSONArray();

        for (Ad ad: objects) {
            jsonArray.put(this.objectToJson(ad));
        }

        return jsonArray;
    }

    @Override
    public Ad get(int serverId) {
        Ad result = null;

        try {
            JSONObject jsonObject = RestClientBase.getInstance(this.getContext())
                    .getJSONRequest(AdWebServiceAdapter.ADS + "/" + String.valueOf(serverId)).get(30, TimeUnit.SECONDS);

            result = this.jsonToObject(jsonObject, true);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Log.e(AdWebServiceAdapter.class.getName(), e.getMessage());
        }

        return result;
    }

    @Override
    public ArrayList<Ad> getAll() {
        ArrayList<Ad> result = new ArrayList<>();

        try {
            JSONObject jsonObject = RestClientBase.getInstance(
                    this.getContext()).getJSONRequest(AdWebServiceAdapter.ADS).get(30, TimeUnit.SECONDS);

            result = this.jsonArrayToObject((JSONArray)jsonObject.get("data"));

            this.adSQLiteAdapter.insertOrUpdate(result);

        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Log.e(AdWebServiceAdapter.class.getName(), e.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    public ArrayList<Ad> getAll(String type) {
        ArrayList<Ad> result = new ArrayList<>();

        try {
            JSONObject jsonObject = RestClientBase.getInstance(
                    this.getContext()).getJSONRequest(AdWebServiceAdapter.ADS + "?type=" + type).get(30, TimeUnit.SECONDS);

            result = this.jsonArrayToObject((JSONArray)jsonObject.get("data"));

            this.adSQLiteAdapter.insertOrUpdate(result);

        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Log.e(AdWebServiceAdapter.class.getName(), e.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public void post(Ad model) {
        JSONObject postObject = this.objectToJson(model);

        RestClientBase.getInstance(
                this.getContext()).postJSONRequest(AdWebServiceAdapter.ADS, postObject);
    }

    @Override
    public void put(Ad model) {
        JSONObject putObject = this.objectToJson(model);

        RestClientBase.getInstance(
                this.getContext()).putJSONRequest(AdWebServiceAdapter.ADS + "/" + String.valueOf(model.getServerId()), putObject);
    }

    @Override
    public void delete(int serverId) {
        RestClientBase.getInstance(
                this.getContext()).deleteJSONRequest(AdWebServiceAdapter.ADS + "/" + String.valueOf(serverId));

    }
}
