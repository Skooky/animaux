package com.findmehome.findmehome.data.base;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.findmehome.findmehome.data.contract.AddressContract;
import com.findmehome.findmehome.data.contract.AnimalContract;
import com.findmehome.findmehome.data.contract.UserContract;
import com.findmehome.findmehome.data.contract.AdContract;

/**
 * Created by maxime on 13/06/2017.
 */

public class FindMeHomeSQLiteOpenHelper extends SQLiteOpenHelper {
    /** Nom de la base de données dans le téléphone. À NE PAS CHANGER. */
    private static final String DATABASE_NAME = "db.sqlite";
    /** Version actuelle de la base de données. En cas de changement dans la base, augmenter sa version. */
    private static final int DATABASE_VERSION = 1;
    /** Instance static de l'open helper. */
    private static FindMeHomeSQLiteOpenHelper instance;
    /** Base de données. */
    private static SQLiteDatabase database;

    /**
     * Get the current instance of the open helper, create one if not exist.
     * @param context {@link Context}
     * @return FindMeHomeSQLiteOpenHelper {@link FindMeHomeSQLiteOpenHelper}
     */
    public static synchronized FindMeHomeSQLiteOpenHelper getInstance(Context context) {
        if (instance == null) {
            instance = new FindMeHomeSQLiteOpenHelper(context);
            database = instance.getWritableDatabase();
        }

        return instance;
    }

    /**
     * Constructor.
     * @param context {@link Context}
     */
    public FindMeHomeSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(new AddressContract().createTable());
        db.execSQL(new UserContract().createTable());
        db.execSQL(new AnimalContract().createTable());
        db.execSQL(new AdContract().createTable());

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
//		Effectuer son changement sous le modèle suivant :
//		db.execSQL("ALTER TABLE .....");
    }
}
