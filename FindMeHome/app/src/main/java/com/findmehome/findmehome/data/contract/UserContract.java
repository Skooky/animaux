package com.findmehome.findmehome.data.contract;

import android.provider.BaseColumns;

import com.findmehome.findmehome.data.contract.base.ContractBase;

/**
 * Created by maxime on 13/06/2017.
 */

public class UserContract extends ContractBase {

    /**
     * Name of the table.
     */
    public static final String TABLE_NAME = "User";

    /**
     * Name of fields DB.
     */
    public static final String FIELD_ISPRO = "is_pro";
    public static final String FIELD_FIRSTNAME = "firstname";
    public static final String FIELD_LASTNAME = "lastname";
    public static final String FIELD_PHONE = "phone";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_SIREN = "siren";
    public static final String FIELD_SOCIETYNAME = "society_name";
    public static final String FIELD_USERNAME = "username";
    public static final String FIELD_ADDRESS = "address_id";

    /**
     * Columns.
     */
    public static final String[] COLUMNS = {
            UserContract.TABLE_NAME + "." + UserContract.FIELD_ID + " AS " + BaseColumns._ID,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_SERVERID,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_ISPRO,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_FIRSTNAME,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_LASTNAME,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_PHONE,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_EMAIL,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_SIREN,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_SOCIETYNAME,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_USERNAME,
            UserContract.TABLE_NAME + "." + UserContract.FIELD_ADDRESS,
    };

    @Override
    public String createTable() {
        return "CREATE TABLE " + UserContract.TABLE_NAME + " ( " +
                UserContract.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                UserContract.FIELD_SERVERID + " INTEGER, " +
                UserContract.FIELD_CREATED_DATE + " DATETIME, " +
                UserContract.FIELD_UPDATED_DATE + " DATETIME, " +
                UserContract.FIELD_SYNC_DATE + " DATETIME, " +
                UserContract.FIELD_ISPRO + " INTEGER NOT NULL, " +
                UserContract.FIELD_FIRSTNAME + " TEXT NOT NULL, " +
                UserContract.FIELD_LASTNAME + " TEXT NOT NULL, " +
                UserContract.FIELD_PHONE + " TEXT NOT NULL, " +
                UserContract.FIELD_EMAIL + " TEXT NOT NULL, " +
                UserContract.FIELD_SIREN + " TEXT, " +
                UserContract.FIELD_SOCIETYNAME + " TEXT, " +
                UserContract.FIELD_USERNAME + " TEXT, " +
                UserContract.FIELD_ADDRESS + " INTEGER, " +
                "FOREIGN KEY (" + FIELD_ADDRESS + ") REFERENCES " +
                AddressContract.TABLE_NAME + "(" + AddressContract.FIELD_ID + ") " +
                ");";
    }
}
