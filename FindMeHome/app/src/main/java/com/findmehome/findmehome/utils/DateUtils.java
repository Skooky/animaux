package com.findmehome.findmehome.utils;

import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Utils class for date format.
 *
 * @author sbristiel
 */

public class DateUtils {

    /**
     * Format simple date.
     */
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * Format date time.
     */
    private static DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    /**
     * Format date format yyyy-MM-dd
     * @param date {@link Date}
     * @return String
     */
    public static String formatDateToString(Date date) {
        String result = null;

        if (date != null) {
            result = simpleDateFormat.format(date);
        }

        return result;
    }

    /**
     * Format date format EEEE dd MMMM yyyy.
     * @param date {@link Date}
     * @return String
     */
    public static String formatDateToCompleteString(Date date) {
        String result = "";

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd MMMM yyyy", Locale.getDefault());

        if (date != null) {
            result = dateFormat.format(date);
        }

        return result.substring(0, 1).toUpperCase() + result.substring(1);
    }

    /**
     * Format {@link DateTime} format yyyy-MM-dd HH:mm:ss
     * @param dateTime {@link DateTime}
     * @return String
     */
    public static String formatDateTimeToString(DateTime dateTime) {
        String result = null;

        if (dateTime != null) {
            result = dateTime.toString(dateTimeFormat);
        }

        return result;
    }

    /**
     * Format date. Format ask : yyyy-MM-dd
     * @param date String
     * @return Date
     */
    public static Date formatStringToDate(String date) {
        Date result = null;

        try {
            result = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            Log.e(DateUtils.class.getName(), e.getMessage());
        }

        return result;
    }

    /**
     * Format {@link DateTime}. Format ask : yyyy-MM-dd HH:mm:ss
     * @param datetime String
     * @return Date
     */
    public static DateTime formatStringToDateTime(String datetime) {
        return dateTimeFormat.parseDateTime(datetime);
    }


}

