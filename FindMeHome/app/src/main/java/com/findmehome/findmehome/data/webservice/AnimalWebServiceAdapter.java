package com.findmehome.findmehome.data.webservice;

import android.content.Context;
import android.util.Log;

import com.findmehome.findmehome.data.contract.AnimalContract;
import com.findmehome.findmehome.data.webservice.base.RestClientBase;
import com.findmehome.findmehome.data.webservice.base.WebServiceAdapterBase;
import com.findmehome.findmehome.entity.Animal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by sandra on 13/06/17.
 */

public class AnimalWebServiceAdapter extends WebServiceAdapterBase<Animal> {

    /** Address for the entity. */
    private static final String ANIMALS = "animals";

    /**
     * Constructor.
     * @param context {@link Context}
     */
    public AnimalWebServiceAdapter(Context context) {
        super(context);
    }


    @Override
    public Animal jsonToObject(JSONObject jsonObject, boolean isData) {
        Animal result = new Animal();

        try {
            JSONObject object = jsonObject;

            if (isData) {
                object = (JSONObject) jsonObject.get("data");
            }

            result.setServerId(object.getInt("id"));
            result.setAge(object.getString(AnimalContract.FIELD_AGE));
            result.setChild(object.getInt(AnimalContract.FIELD_CHILD));
            result.setColor(object.getInt(AnimalContract.FIELD_COLOR));
            result.setDewormed(object.getBoolean(AnimalContract.FIELD_ISDEWORMED));
            result.setHealth(object.getInt(AnimalContract.FIELD_HEALTH));
            result.setIdentification(object.getString(AnimalContract.FIELD_IDENTIFICATION));
            result.setMotherIdentification(object.getString(AnimalContract.FIELD_MOTHERIDENTIFICATION));
            result.setNature(object.getString(AnimalContract.FIELD_NATURE));
            result.setNbWorn(object.getInt(AnimalContract.FIELD_NBWORN));
            result.setPuced(object.getBoolean(AnimalContract.FIELD_ISPUCED));
            result.setRace(object.getInt(AnimalContract.FIELD_RACE));
            result.setSex(object.getInt(AnimalContract.FIELD_SEX));
            result.setSterilized(object.getBoolean(AnimalContract.FIELD_ISSTERILIZED));
            result.setTatoo(object.getBoolean(AnimalContract.FIELD_ISTATOO));
            result.setVaccined(object.getBoolean(AnimalContract.FIELD_ISVACCINED));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public ArrayList<Animal> jsonArrayToObject(JSONArray jsonArray) {
        ArrayList<Animal> result = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                result.add(this.jsonToObject((JSONObject) jsonArray.get(i), false));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    @Override
    public JSONObject objectToJson(Animal object) {
        JSONObject result = new JSONObject();

        try {
            result.put(AnimalContract.FIELD_AGE, object.getAge());
            result.put(AnimalContract.FIELD_CHILD, object.getChild());
            result.put(AnimalContract.FIELD_COLOR, object.getColor());
            result.put(AnimalContract.FIELD_ISDEWORMED, object.isDewormed());
            result.put(AnimalContract.FIELD_HEALTH, object.getHealth());
            result.put(AnimalContract.FIELD_IDENTIFICATION, object.getIdentification());
            result.put(AnimalContract.FIELD_MOTHERIDENTIFICATION, object.getMotherIdentification());
            result.put(AnimalContract.FIELD_NATURE, object.getNature());
            result.put(AnimalContract.FIELD_NBWORN, object.getNbWorn());
            result.put(AnimalContract.FIELD_ISPUCED, object.isPuced());
            result.put(AnimalContract.FIELD_RACE, object.getRace());
            result.put(AnimalContract.FIELD_SEX, object.getSex());
            result.put(AnimalContract.FIELD_ISSTERILIZED, object.isSterilized());
            result.put(AnimalContract.FIELD_ISTATOO, object.isTatoo());
            result.put(AnimalContract.FIELD_ISVACCINED, object.isVaccined());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public JSONArray objectToJsonArray(ArrayList<Animal> objects) {
        JSONArray jsonArray = new JSONArray();

        for (Animal user : objects) {
            jsonArray.put(this.objectToJson(user));
        }

        return jsonArray;
    }

    @Override
    public Animal get(int serverId) {
        Animal result = null;

        try {
            JSONObject jsonObject = RestClientBase.getInstance(this.getContext())
                    .getJSONRequest(ANIMALS+"/"+String.valueOf(serverId)).get(30, TimeUnit.SECONDS);

            result = this.jsonToObject(jsonObject, true);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Log.e(AnimalWebServiceAdapter.class.getName(), e.getMessage());
        }

        return result;
    }

    @Override
    public ArrayList<Animal> getAll() {
        ArrayList<Animal> result = new ArrayList<>();

        try {
            JSONObject jsonObject = RestClientBase.getInstance(
                    this.getContext()).getJSONRequest(ANIMALS).get(30, TimeUnit.SECONDS);

            result = this.jsonArrayToObject(jsonObject.getJSONArray("data"));

        } catch (InterruptedException | ExecutionException | TimeoutException | JSONException e) {
            Log.e(AnimalWebServiceAdapter.class.getName(), e.getMessage());
        }

        return result;
    }

    @Override
    public void post(Animal model) {
        JSONObject postObject = this.objectToJson(model);

        RestClientBase.getInstance(
                this.getContext()).postJSONRequest(ANIMALS, postObject);
    }

    @Override
    public void put(Animal model) {
        JSONObject putObject = this.objectToJson(model);

        RestClientBase.getInstance(
                this.getContext()).putJSONRequest(ANIMALS+"/" + String.valueOf(model.getServerId()), putObject);
    }

    @Override
    public void delete(int serverId) {
        RestClientBase.getInstance(
                this.getContext()).deleteJSONRequest(ANIMALS+"/"+String.valueOf(serverId));

    }
}
