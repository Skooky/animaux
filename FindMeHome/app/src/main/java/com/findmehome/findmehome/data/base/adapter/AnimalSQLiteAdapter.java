package com.findmehome.findmehome.data.base.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.findmehome.findmehome.data.base.adapter.base.SQLiteAdapterBase;
import com.findmehome.findmehome.data.contract.AnimalContract;
import com.findmehome.findmehome.entity.Animal;
import com.findmehome.findmehome.entity.Animal;
import com.findmehome.findmehome.utils.DateUtils;
import com.findmehome.findmehome.utils.PreferencesUtils;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by maxime on 13/06/2017.
 */

public class AnimalSQLiteAdapter extends SQLiteAdapterBase<Animal> {

    /**
     * Constructor.
     *
     * @param context Le context
     */
    public AnimalSQLiteAdapter(Context context) {
        super(context);
    }

    @Override
    public long insert(Animal model) {
        this.open();

        long result = this.db.insert(AnimalContract.TABLE_NAME, null, this.itemToContentValues(model));

        this.close();

        return result;
    }

    @Override
    public void update(Animal model) {
        this.open();

        model.setUpdatedDate(new DateTime());

        this.db.update(
                AnimalContract.TABLE_NAME,
                this.itemToContentValues(model),
                AnimalContract.FIELD_ID + " = ?",
                new String[] {String.valueOf(model.getId())});

        this.close();
    }

    @Override
    public long insertOrUpdate(Animal model) {
        Animal dbAnimal = this.getWithServerId(model.getServerId());
        long result = -1;

        if (dbAnimal != null) {
            model.setId(dbAnimal.getId());
            model.setSyncDate(PreferencesUtils.getSyncDate(this.getContext()));

            this.update(model);
        } else {
            result = this.insert(model);
        }

        return result;
    }

    @Override
    public void insertOrUpdate(ArrayList<Animal> models) {
        for (Animal Animal : models) {
            this.insertOrUpdate(Animal);
        }
    }

    @Override
    public void delete(Animal model) {
        this.deleteWithId(model.getId());
    }

    @Override
    public void deleteWithId(int modelId) {
        this.open();

        this.db.delete(AnimalContract.TABLE_NAME, AnimalContract.FIELD_ID + " = ?", new String[] {String.valueOf(modelId)});

        this.close();
    }

    @Override
    public Animal get(Animal model) {
        return this.getWithId(model.getId());
    }

    @Override
    public Animal getWithId(int modelId) {
        this.open();
        Animal result = null;

        Cursor cursor = this.query(
                AnimalContract.FIELD_ID + " = ?",
                new String[] {String.valueOf(modelId)},
                null,
                null,
                null,
                null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            result = this.cursorToItem(cursor);

            cursor.close();
        }

        this.close();

        return result;
    }

    @Override
    public Animal getWithServerId(int modelServerId) {
        this.open();
        Animal result = null;

        Cursor cursor = this.query(
                AnimalContract.FIELD_SERVERID + " = ?",
                new String[] {String.valueOf(modelServerId)},
                null,
                null,
                null,
                null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            result = this.cursorToItem(cursor);

            cursor.close();
        }

        this.close();

        return result;
    }

    @Override
    public ArrayList<Animal> getAll() {
        this.open();

        ArrayList<Animal> result;

        Cursor cursor = this.query(null, null, null, null, null, null);

        result = this.cursorToItems(cursor);

        this.close();
        return result;
    }

    @Override
    public ContentValues itemToContentValues(Animal item) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(AnimalContract.FIELD_SERVERID, item.getServerId());

        contentValues.put(AnimalContract.FIELD_AGE, item.getAge());
        contentValues.put(AnimalContract.FIELD_ISVACCINED, item.isVaccined());
        contentValues.put(AnimalContract.FIELD_ISPUCED, item.isPuced());
        contentValues.put(AnimalContract.FIELD_ISSTERILIZED, item.isSterilized());
        contentValues.put(AnimalContract.FIELD_ISDEWORMED, item.isDewormed());
        contentValues.put(AnimalContract.FIELD_ISTATOO, item.isTatoo());
        contentValues.put(AnimalContract.FIELD_NBWORN, item.getNbWorn());
        contentValues.put(AnimalContract.FIELD_IDENTIFICATION, item.getIdentification());
        contentValues.put(AnimalContract.FIELD_MOTHERIDENTIFICATION, item.getMotherIdentification());
        contentValues.put(AnimalContract.FIELD_SEX, item.getSex());
        contentValues.put(AnimalContract.FIELD_COLOR, item.getColor());
        contentValues.put(AnimalContract.FIELD_RACE, item.getRace());
        contentValues.put(AnimalContract.FIELD_HEALTH, item.getHealth());
        contentValues.put(AnimalContract.FIELD_NATURE, item.getNature());
        contentValues.put(AnimalContract.FIELD_CHILD, item.getChild());

        if (item.getCreatedDate() == null) {
            item.setCreatedDate(new DateTime());
        }

        contentValues.put(AnimalContract.FIELD_CREATED_DATE,
                DateUtils.formatDateTimeToString(item.getCreatedDate()));

        if (item.getUpdatedDate() != null) {
            contentValues.put(AnimalContract.FIELD_UPDATED_DATE,
                    DateUtils.formatDateTimeToString(item.getUpdatedDate()));
        }

        if (item.getSyncDate() != null) {
            contentValues.put(AnimalContract.FIELD_SYNC_DATE,
                    DateUtils.formatDateTimeToString(item.getSyncDate()));
        }

        return contentValues;
    }

    @Override
    public Animal cursorToItem(Cursor cursor) {
        Animal result = new Animal();
        int column;

        if ((column = cursor.getColumnIndex(BaseColumns._ID)) > -1) {
            result.setId(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_SERVERID)) > -1) {
            result.setServerId(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_AGE)) > -1) {
            result.setAge(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_ISVACCINED)) > -1) {
            result.setVaccined(cursor.getInt(column) == 1);
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_ISPUCED)) > -1) {
            result.setPuced(cursor.getInt(column) == 1);
        }
        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_ISSTERILIZED)) > -1) {
            result.setSterilized(cursor.getInt(column) == 1);
        }
        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_ISDEWORMED)) > -1) {
            result.setDewormed(cursor.getInt(column) == 1);
        }
        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_ISTATOO)) > -1) {
            result.setTatoo(cursor.getInt(column) == 1);
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_NBWORN)) > -1) {
            result.setNbWorn(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_IDENTIFICATION)) > -1) {
            result.setIdentification(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_MOTHERIDENTIFICATION)) > -1) {
            result.setMotherIdentification(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_SEX)) > -1) {
            result.setSex(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_COLOR)) > -1) {
            result.setColor(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_RACE)) > -1) {
            result.setRace(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_HEALTH)) > -1) {
            result.setHealth(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_NATURE)) > -1) {
            result.setNature(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_CHILD)) > -1) {
            result.setChild(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_CREATED_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setCreatedDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_UPDATED_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setUpdatedDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        if ((column = cursor.getColumnIndex(AnimalContract.FIELD_SYNC_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setSyncDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        return result;
    }

    @Override
    public ArrayList<Animal> cursorToItems(Cursor cursor) {
        ArrayList<Animal> result = new ArrayList<>();

        if (cursor.getCount() != 0) {
            while (cursor.moveToNext()) {
                result.add(this.cursorToItem(cursor));
            }

            cursor.close();
        }

        return result;
    }

    @Override
    public Cursor query(
            String whereClause,
            String[] whereArgs,
            String groupBy,
            String having,
            String orderBy,
            String limit) {
        return this.db.query(
                AnimalContract.TABLE_NAME,
                AnimalContract.COLUMNS,
                whereClause,
                whereArgs,
                groupBy,
                having,
                orderBy,
                limit);
    }

    @Override
    public void deleteUnused() {
        this.db.delete(
                AnimalContract.TABLE_NAME,
                AnimalContract.FIELD_SYNC_DATE + " <> ?",
                new String[] { String.valueOf(PreferencesUtils.getSyncDate(this.getContext()).getMillis())}
        );
    }
}
