package com.findmehome.findmehome.entity;

/**
 * Created by sandra on 25/06/17.
 */

public enum Color {
    WHITE("Blanc", 1),
    BLACK("Noir", 2),
    ORANGE("Roux", 3),
    BLOND("Blond", 4),
    GREY("Gris", 5);

    private String stringValue;
    private int intValue;

    private Color(String toString, int value) {
        stringValue = toString;
        intValue = value;
    }

    public int getIntValue() {
        return intValue;
    }

    @Override
    public String toString() {
        return stringValue;
    }
}
