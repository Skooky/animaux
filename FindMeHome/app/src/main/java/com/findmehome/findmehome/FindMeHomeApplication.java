package com.findmehome.findmehome;

import android.app.Application;
import android.content.Context;

import com.findmehome.findmehome.data.base.FindMeHomeSQLiteOpenHelper;

/**
 * Created by maxime on 13/06/2017.
 */

public class FindMeHomeApplication extends Application {
    public static Context context;

    /**
     * Constructor.
     */
    public FindMeHomeApplication() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialise la base de données au lancement de l'application.
        FindMeHomeSQLiteOpenHelper.getInstance(this);
        context = getApplicationContext();
    }
}
