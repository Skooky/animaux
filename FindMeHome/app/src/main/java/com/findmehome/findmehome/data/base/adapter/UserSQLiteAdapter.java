package com.findmehome.findmehome.data.base.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.findmehome.findmehome.data.base.adapter.base.SQLiteAdapterBase;
import com.findmehome.findmehome.data.contract.UserContract;
import com.findmehome.findmehome.entity.User;
import com.findmehome.findmehome.entity.User;
import com.findmehome.findmehome.utils.DateUtils;
import com.findmehome.findmehome.utils.PreferencesUtils;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by maxime on 13/06/2017.
 */

public class UserSQLiteAdapter extends SQLiteAdapterBase<User> {

    /**
     * Constructor.
     *
     * @param context Le context
     */
    public UserSQLiteAdapter(Context context) {
        super(context);
    }

    @Override
    public long insert(User model) {
        this.open();

        long result = this.db.insert(UserContract.TABLE_NAME, null, this.itemToContentValues(model));

        this.close();

        return result;
    }

    @Override
    public void update(User model) {
        this.open();

        model.setUpdatedDate(new DateTime());

        this.db.update(
                UserContract.TABLE_NAME,
                this.itemToContentValues(model),
                UserContract.FIELD_ID + " = ?",
                new String[] {String.valueOf(model.getId())});

        this.close();
    }

    @Override
    public long insertOrUpdate(User model) {
        User dbUser = this.getWithServerId(model.getServerId());
        long result = -1;

        if (dbUser != null) {
            model.setId(dbUser.getId());
            model.setSyncDate(PreferencesUtils.getSyncDate(this.getContext()));

            this.update(model);
        } else {
            result = this.insert(model);
        }

        return result;
    }

    @Override
    public void insertOrUpdate(ArrayList<User> models) {
        for (User User : models) {
            this.insertOrUpdate(User);
        }
    }

    @Override
    public void delete(User model) {
        this.deleteWithId(model.getId());
    }

    @Override
    public void deleteWithId(int modelId) {
        this.open();

        this.db.delete(UserContract.TABLE_NAME, UserContract.FIELD_ID + " = ?", new String[] {String.valueOf(modelId)});

        this.close();
    }

    @Override
    public User get(User model) {
        return this.getWithId(model.getId());
    }

    @Override
    public User getWithId(int modelId) {
        this.open();
        User result = null;

        Cursor cursor = this.query(
                UserContract.FIELD_ID + " = ?",
                new String[] {String.valueOf(modelId)},
                null,
                null,
                null,
                null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            result = this.cursorToItem(cursor);

            cursor.close();
        }

        this.close();

        return result;
    }

    @Override
    public User getWithServerId(int modelServerId) {
        this.open();
        User result = null;

        Cursor cursor = this.query(
                UserContract.FIELD_SERVERID + " = ?",
                new String[] {String.valueOf(modelServerId)},
                null,
                null,
                null,
                null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            result = this.cursorToItem(cursor);

            cursor.close();
        }

        this.close();

        return result;
    }

    @Override
    public ArrayList<User> getAll() {
        this.open();

        ArrayList<User> result;

        Cursor cursor = this.query(null, null, null, null, null, null);

        result = this.cursorToItems(cursor);

        this.close();
        return result;
    }

    @Override
    public ContentValues itemToContentValues(User item) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(UserContract.FIELD_SERVERID, item.getServerId());

        contentValues.put(UserContract.FIELD_ISPRO, item.isPro());
        contentValues.put(UserContract.FIELD_USERNAME, item.getUserName());
        contentValues.put(UserContract.FIELD_FIRSTNAME, item.getFirstName());
        contentValues.put(UserContract.FIELD_LASTNAME, item.getLastName());
        contentValues.put(UserContract.FIELD_PHONE, item.getPhone());
        contentValues.put(UserContract.FIELD_EMAIL, item.getEmail());
        contentValues.put(UserContract.FIELD_SIREN, item.getSiren());
        contentValues.put(UserContract.FIELD_SOCIETYNAME, item.getSocietyName());

        if (item.getAddress() != null) {
            contentValues.put(UserContract.FIELD_ADDRESS, item.getAddress().getId());
        }

        if (item.getCreatedDate() == null) {
            item.setCreatedDate(new DateTime());
        }

        contentValues.put(UserContract.FIELD_CREATED_DATE,
                DateUtils.formatDateTimeToString(item.getCreatedDate()));

        if (item.getUpdatedDate() != null) {
            contentValues.put(UserContract.FIELD_UPDATED_DATE,
                    DateUtils.formatDateTimeToString(item.getUpdatedDate()));
        }

        if (item.getSyncDate() != null) {
            contentValues.put(UserContract.FIELD_SYNC_DATE,
                    DateUtils.formatDateTimeToString(item.getSyncDate()));
        }

        return contentValues;
    }

    @Override
    public User cursorToItem(Cursor cursor) {
        User result = new User();
        int column;

        if ((column = cursor.getColumnIndex(BaseColumns._ID)) > -1) {
            result.setId(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_SERVERID)) > -1) {
            result.setServerId(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_ISPRO)) > -1) {
            result.setPro(cursor.getInt(column) == 1);
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_USERNAME)) > -1) {
            result.setUserName(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_FIRSTNAME)) > -1) {
            result.setFirstName(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_LASTNAME)) > -1) {
            result.setLastName(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_PHONE)) > -1) {
            result.setPhone(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_EMAIL)) > -1) {
            result.setEmail(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_SIREN)) > -1) {
            result.setSiren(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_SOCIETYNAME)) > -1) {
            result.setSocietyName(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_ADDRESS)) > -1) {
            result.setAddress(new AddressSQLiteAdapter(this.context).getWithId(cursor.getInt(column)));
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_CREATED_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setCreatedDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_UPDATED_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setUpdatedDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        if ((column = cursor.getColumnIndex(UserContract.FIELD_SYNC_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setSyncDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        return result;
    }

    @Override
    public ArrayList<User> cursorToItems(Cursor cursor) {
        ArrayList<User> result = new ArrayList<>();

        if (cursor.getCount() != 0) {
            while (cursor.moveToNext()) {
                result.add(this.cursorToItem(cursor));
            }

            cursor.close();
        }

        return result;
    }

    @Override
    public Cursor query(
            String whereClause,
            String[] whereArgs,
            String groupBy,
            String having,
            String orderBy,
            String limit) {
        return this.db.query(
                UserContract.TABLE_NAME,
                UserContract.COLUMNS,
                whereClause,
                whereArgs,
                groupBy,
                having,
                orderBy,
                limit);
    }

    @Override
    public void deleteUnused() {
        this.db.delete(
                UserContract.TABLE_NAME,
                UserContract.FIELD_SYNC_DATE + " <> ?",
                new String[] { String.valueOf(PreferencesUtils.getSyncDate(this.getContext()).getMillis())}
        );
    }
}
