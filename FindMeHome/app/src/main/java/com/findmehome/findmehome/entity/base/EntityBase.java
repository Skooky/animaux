package com.findmehome.findmehome.entity.base;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created by maxime on 13/06/2017.
 */

public class EntityBase implements Serializable, Parcelable {
    /**
     * Id.
     */
    private int id;

    /**
     * ServerId
     */
    private int serverId;

    /** Create date. */
    private DateTime createdDate;
    /** Update date. */
    private DateTime updatedDate;
    /** Synchronization date. */
    private DateTime syncDate;

    /**
     * Constructor of parcelable.
     * @param in {@link Parcel}
     */
    protected EntityBase(Parcel in) {
        id = in.readInt();
        serverId = in.readInt();
        createdDate = new DateTime(in.readLong());
        updatedDate = new DateTime(in.readLong());
        syncDate = new DateTime(in.readLong());
    }

    /**
     * Constructor.
     */
    public EntityBase() {
    }

    /**
     * Get the id.
     * @return int id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Set the id.
     * @param id int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Get the serverId.
     * @return int
     */
    public int getServerId() {
        return serverId;
    }

    /**
     * Set the serverId.
     * @param serverId
     */
    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    /**
     * Get createdDate.
     * @return DateTime
     */
    public DateTime getCreatedDate() {
        return createdDate;
    }

    /**
     * Set Create Date
     * @param createdDate DateTime
     */
    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Get updateDate.
     * @return DateTime
     */
    public DateTime getUpdatedDate() {
        return updatedDate;
    }

    /**
     * Set Update date
     * @param updatedDate DateTime
     */
    public void setUpdatedDate(DateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    /**
     * Get syncDate.
     * @return DateTime
     */
    public DateTime getSyncDate() {
        return syncDate;
    }

    /**
     * Set SyncDAte
     * @param syncDate DateTime
     */
    public void setSyncDate(DateTime syncDate) {
        this.syncDate = syncDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(serverId);

        if (this.createdDate != null) {
            parcel.writeLong(this.createdDate.getMillis());
        }

        if (this.updatedDate != null) {
            parcel.writeLong(this.updatedDate.getMillis());
        }

        if (this.syncDate != null) {
            parcel.writeLong(this.syncDate.getMillis());
        }
    }

    /**
     * Parcelable creator.
     */
    public static final Creator<EntityBase> CREATOR = new Creator<EntityBase>() {
        @Override
        public EntityBase createFromParcel(Parcel in) {
            return new EntityBase(in);
        }

        @Override
        public EntityBase[] newArray(int size) {
            return new EntityBase[size];
        }
    };
}
