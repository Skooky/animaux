package com.findmehome.findmehome.entity;

import com.findmehome.findmehome.entity.base.EntityBase;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by maxime on 13/06/2017.
 */

public class User extends EntityBase implements Parcelable, Serializable {

    private boolean isPro;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private String siren;
    private String societyName;
    private String userName;
    private Address address;

    public User() {

    }

    public boolean isPro() {
        return isPro;
    }

    public void setPro(boolean pro) {
        isPro = pro;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSiren() {
        return siren;
    }

    public void setSiren(String siren) {
        this.siren = siren;
    }

    public String getSocietyName() {
        return societyName;
    }

    public void setSocietyName(String societyName) {
        this.societyName = societyName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /** Parcelable methods. **/
    protected User(Parcel in) {
        super(in);

        this.isPro = in.readByte() != 0;
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.phone = in.readString();
        this.email = in.readString();
        this.siren = in.readString();
        this.societyName = in.readString();
        this.userName = in.readString();
        this.address = in.readParcelable(Address.class.getClassLoader());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeByte((byte) (isPro ? 1 : 0));
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(phone);
        parcel.writeString(email);
        parcel.writeString(siren);
        parcel.writeString(societyName);
        parcel.writeString(userName);
        parcel.writeParcelable(address, i);
    }


}
