package com.findmehome.findmehome.view.Ad;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.findmehome.findmehome.R;
import com.findmehome.findmehome.data.base.adapter.AdSQLiteAdapter;
import com.findmehome.findmehome.data.base.adapter.AnimalSQLiteAdapter;
import com.findmehome.findmehome.data.contract.AdContract;
import com.findmehome.findmehome.entity.Ad;
import com.findmehome.findmehome.entity.Color;
import com.findmehome.findmehome.entity.Race;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdShowFragment extends Fragment {

    private TextView idView;
    private TextView titleView;
    private TextView contentView;
    private TextView sexView;
    private TextView ageView;
    private CheckBox vaccinedBox;
    private CheckBox pucedBox;
    private CheckBox sterilizedBox;
    private CheckBox tatooedBox;
    private CheckBox wormedBox;
    private TextView wornView;
    private TextView colorView;
    private TextView raceView;
    private TextView identificationView;
    private TextView motherIdentificationView;
    private TextView healthView;
    private TextView descriptionView;

    private Ad model;

    public AdShowFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ad_show, container, false);

        this.model = this.getActivity().getIntent().getBundleExtra(AdContract.TABLE_NAME)
                .getParcelable(AdContract.TABLE_NAME);

        this.model = new AdSQLiteAdapter(this.getActivity()).getWithId(this.model.getId());
        this.model.setAnimal(new AnimalSQLiteAdapter(this.getActivity()).getWithId(this.model.getAnimal().getId()));

        this.initializeView(view);

        return view;
    }

    /**
     * Initialize view.
     * @param view View
     */
    private void initializeView(View view) {
        this.idView = (TextView) view.findViewById(R.id.fragment_ad_show_id);
        this.titleView = (TextView) view.findViewById(R.id.fragment_ad_show_title);
        this.contentView = (TextView) view.findViewById(R.id.fragment_ad_show_content);
        this.ageView = (TextView) view.findViewById(R.id.fragment_ad_show_age);
        this.colorView = (TextView) view.findViewById(R.id.fragment_ad_show_color);
        this.descriptionView = (TextView) view.findViewById(R.id.fragment_ad_show_description);
        this.healthView = (TextView) view.findViewById(R.id.fragment_ad_show_health);
        this.identificationView = (TextView) view.findViewById(R.id.fragment_ad_show_identification);
        this.motherIdentificationView = (TextView) view.findViewById(R.id.fragment_ad_show_mother_identification);
        this.vaccinedBox = (CheckBox) view.findViewById(R.id.fragment_ad_show_vaccined_box);
        this.tatooedBox = (CheckBox) view.findViewById(R.id.fragment_ad_show_tatooed_box);
        this.sterilizedBox = (CheckBox) view.findViewById(R.id.fragment_ad_show_sterilized_box);
        this.wormedBox = (CheckBox) view.findViewById(R.id.fragment_ad_show_wormed_box);
        this.pucedBox = (CheckBox) view.findViewById(R.id.fragment_ad_show_puced_box);
        this.wornView = (TextView) view.findViewById(R.id.fragment_ad_show_worn);
        this.raceView = (TextView) view.findViewById(R.id.fragment_ad_show_race);

        this.idView.setText(String.valueOf(this.model.getId()));
        this.titleView.setText(this.model.getTitle());
        this.contentView.setText(this.model.getContent());

        this.ageView.setText(this.model.getAnimal().getAge());
        this.colorView.setText(Color.values()[this.model.getAnimal().getColor()].toString());
        this.descriptionView.setText(this.model.getAnimal().getNature());
        this.healthView.setText(this.model.getAnimal().getHealth());
        this.identificationView.setText(this.model.getAnimal().getIdentification());
        this.motherIdentificationView.setText(this.model.getAnimal().getMotherIdentification());
        this.vaccinedBox.setChecked(this.model.getAnimal().isVaccined());
        this.pucedBox.setChecked(this.model.getAnimal().isPuced());
        this.sterilizedBox.setChecked(this.model.getAnimal().isSterilized());
        this.wormedBox.setChecked(this.model.getAnimal().isDewormed());
        this.tatooedBox.setChecked(this.model.getAnimal().isTatoo());
        this.wornView.setText(this.model.getAnimal().getNbWorn());
        this.raceView.setText(Race.values()[this.model.getAnimal().getRace()].toString());
    }
}
