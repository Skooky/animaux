package com.findmehome.findmehome;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

import com.findmehome.findmehome.data.contract.AdContract;
import com.findmehome.findmehome.data.webservice.AdWebServiceAdapter;
import com.findmehome.findmehome.data.webservice.UserWebServiceAdapter;
import com.findmehome.findmehome.entity.Ad;
import com.findmehome.findmehome.utils.PreferencesUtils;
import com.findmehome.findmehome.view.Ad.AdCreateActivity;
import com.findmehome.findmehome.view.Ad.AdListFragment;
import com.findmehome.findmehome.view.Ad.AdShowActivity;
import com.findmehome.findmehome.view.Login.LoginActivity;
import com.findmehome.findmehome.view.Login.SigninActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        AdListFragment.OnListFragmentInteractionListener, TabHost.OnTabChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabHost host = (TabHost)findViewById(R.id.tabHost);
        host.setup();
        host.setOnTabChangedListener(this);

        //Tab Dons
        TabHost.TabSpec spec = host.newTabSpec("Dons");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Dons");
        host.addTab(spec);

        //Tab Ventes
        spec = host.newTabSpec("Ventes");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Ventes");
        host.addTab(spec);

        //Tab Trouvés
        spec = host.newTabSpec("Trouvés");
        spec.setContent(R.id.tab3);
        spec.setIndicator("Trouvés");
        host.addTab(spec);

        //Tab Saillies
        spec = host.newTabSpec("Saillies");
        spec.setContent(R.id.tab4);
        spec.setIndicator("Saillies");
        host.addTab(spec);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;

                if (TextUtils.isEmpty(PreferencesUtils.getToken(MainActivity.this))) {
                    intent = new Intent(MainActivity.this, LoginActivity.class);
                } else {
                    intent = new Intent(MainActivity.this, AdCreateActivity.class);
                }

                MainActivity.this.startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent = null;

        if (id == R.id.nav_login) {
            intent = new Intent(this, LoginActivity.class);
        } else if (id == R.id.nav_signin) {
            intent = new Intent(this, SigninActivity.class);
        } else if (id == R.id.nav_account) {

        } else if (id == R.id.nav_ads) {

        }

        if (intent != null) {
            this.startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onListFragmentInteraction(Ad item) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AdContract.TABLE_NAME, item);

        Intent intent = new Intent(this, AdShowActivity.class);
        intent.putExtra(AdContract.TABLE_NAME, bundle);

        this.startActivity(intent);
    }

    @Override
    public void onTabChanged(String s) {
        AdListFragment fragment = new AdListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", s);

        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.listFragment, fragment).commit();
    }
}
