package com.findmehome.findmehome.data.webservice;

import android.content.Context;
import android.util.Log;

import com.findmehome.findmehome.data.contract.AddressContract;
import com.findmehome.findmehome.data.webservice.base.RestClientBase;
import com.findmehome.findmehome.data.webservice.base.WebServiceAdapterBase;
import com.findmehome.findmehome.entity.Address;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by sandra on 13/06/17.
 */

public class AddressWebServiceAdapter extends WebServiceAdapterBase<Address> {

    /** Address for the entity. */
    private static final String ADDRESSES = "addresses";

    /**
     * Constructor.
     * @param context {@link Context}
     */
    public AddressWebServiceAdapter(Context context) {
        super(context);
    }

    @Override
    public Address jsonToObject(JSONObject jsonObject, boolean isData) {
        Address result = new Address();

        try {
            JSONObject object = jsonObject;
            if (isData) {
                object = (JSONObject) jsonObject.get("data");
            }

            result.setServerId(object.getInt("id"));
            result.setCity(object.getString(AddressContract.FIELD_CITY));
            result.setStreet(object.getString(AddressContract.FIELD_STREET));
            result.setZipcode(object.getString(AddressContract.FIELD_ZIPCODE));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public ArrayList<Address> jsonArrayToObject(JSONArray jsonArray) {
        ArrayList<Address> result = new ArrayList<>();
        Address address;
        JSONObject object;

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                address = new Address();
                object = (JSONObject) jsonArray.get(i);

                address.setServerId(object.getInt("id"));
                address.setCity(object.getString(AddressContract.FIELD_CITY));
                address.setStreet(object.getString(AddressContract.FIELD_STREET));
                address.setZipcode(object.getString(AddressContract.FIELD_ZIPCODE));

                result.add(address);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    @Override
    public JSONObject objectToJson(Address object) {
        JSONObject result = new JSONObject();

        try {
            result.put(AddressContract.FIELD_CITY, object.getCity());
            result.put(AddressContract.FIELD_STREET, object.getCity());
            result.put(AddressContract.FIELD_ZIPCODE, object.getCity());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public JSONArray objectToJsonArray(ArrayList<Address> objects) {
        JSONArray jsonArray = new JSONArray();

        for (Address address : objects) {
            jsonArray.put(this.objectToJson(address));
        }

        return jsonArray;
    }


    @Override
    public Address get(int serverId) {
        Address result = null;

        try {
            JSONObject jsonObject = RestClientBase.getInstance(this.getContext())
                    .getJSONRequest(ADDRESSES+"/"+String.valueOf(serverId)).get(30, TimeUnit.SECONDS);

            result = this.jsonToObject(jsonObject, true);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Log.e(AddressWebServiceAdapter.class.getName(), e.getMessage());
        }

        return result;
    }

    @Override
    public ArrayList<Address> getAll() {
        ArrayList<Address> result = new ArrayList<>();

        try {
            JSONObject jsonObject = RestClientBase.getInstance(
                    this.getContext()).getJSONRequest(ADDRESSES).get(30, TimeUnit.SECONDS);

            result = this.jsonArrayToObject(jsonObject.getJSONArray("data"));

        } catch (InterruptedException | ExecutionException | TimeoutException | JSONException e) {
            Log.e(AddressWebServiceAdapter.class.getName(), e.getMessage());
        }

        return result;
    }

    @Override
    public void post(Address model) {
        JSONObject postObject = this.objectToJson(model);

        RestClientBase.getInstance(
                this.getContext()).postJSONRequest(ADDRESSES, postObject);
    }

    @Override
    public void put(Address model) {
        JSONObject putObject = this.objectToJson(model);

        RestClientBase.getInstance(
                this.getContext()).putJSONRequest(
                        ADDRESSES+"/"+String.valueOf(model.getServerId()), putObject);
    }

    @Override
    public void delete(int serverId) {
        RestClientBase.getInstance(
                this.getContext()).deleteJSONRequest(ADDRESSES+"/"+String.valueOf(serverId));

    }
}
