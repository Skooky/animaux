package com.findmehome.findmehome.view.Ad;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.findmehome.findmehome.R;
import com.findmehome.findmehome.data.base.adapter.AdSQLiteAdapter;
import com.findmehome.findmehome.data.base.adapter.AnimalSQLiteAdapter;
import com.findmehome.findmehome.data.webservice.AdWebServiceAdapter;
import com.findmehome.findmehome.entity.Ad;
import com.findmehome.findmehome.entity.Animal;
import com.findmehome.findmehome.entity.Color;
import com.findmehome.findmehome.entity.Race;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdCreateFragment extends Fragment implements View.OnClickListener {

    private EditText titleText;
    private EditText contentText;

    //Animal
    private EditText ageText;
    private EditText identificationText;
    private EditText motheridentificationText;
    private RadioButton femaleButton;
    private RadioButton maleButton;
    private CheckBox isVaccinedBox;
    private CheckBox isPucedBox;
    private CheckBox isTatooedBox;
    private CheckBox isSterilizedBox;
    private CheckBox isDewormedBox;
    private CheckBox healthBox;
    private EditText nature;
    private Spinner raceSpinner;
    private Spinner colorSpinner;



    public AdCreateFragment() {
        //TODO Au préalable, demander à l'utilisateur quel type d'annonce il veut créer.
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ad_create, container, false);

        this.initializeView(view);

        return view;
    }

    /**
     * Initialize view.
     * @param view View
     */
    private void initializeView(View view) {
        this.titleText = (EditText) view.findViewById(R.id.ad_create_title);
        this.contentText = (EditText) view.findViewById(R.id.ad_create_content);
        this.ageText = (EditText) view.findViewById(R.id.ad_create_age);
        this.identificationText = (EditText) view.findViewById(R.id.ad_create_identification);
        this.motheridentificationText = (EditText) view.findViewById(R.id.ad_create_mother_identification);
        this.femaleButton = (RadioButton) view.findViewById(R.id.ad_create_sex_f);
        this.maleButton = (RadioButton) view.findViewById(R.id.ad_create_sex_m);
        this.isVaccinedBox = (CheckBox) view.findViewById(R.id.ad_create_vaccined);
        this.isPucedBox = (CheckBox) view.findViewById(R.id.ad_create_puced);
        this.isTatooedBox = (CheckBox) view.findViewById(R.id.ad_create_tatoo);
        this.isSterilizedBox = (CheckBox) view.findViewById(R.id.ad_create_sterilized);
        this.isDewormedBox = (CheckBox) view.findViewById(R.id.ad_create_wormed);
        this.healthBox = (CheckBox) view.findViewById(R.id.ad_create_health);
        this.nature = (EditText) view.findViewById(R.id.ad_create_description);
        this.raceSpinner = (Spinner) view.findViewById(R.id.ad_create_race);
        this.colorSpinner = (Spinner) view.findViewById(R.id.ad_create_color);


        ArrayList<Race> raceArray = new ArrayList<>();
        raceArray.add(Race.DOG);
        raceArray.add(Race.CAT);

        ArrayAdapter<Race> raceArrayAdapter = new ArrayAdapter<Race>(
                this.getActivity(),
                android.R.layout.simple_spinner_item,
                raceArray
        );

        ArrayList<Color> colorArray = new ArrayList<>();
        colorArray.add(Color.WHITE);
        colorArray.add(Color.BLACK);
        colorArray.add(Color.GREY);
        colorArray.add(Color.ORANGE);
        colorArray.add(Color.BLOND);

        ArrayAdapter<Color> colorArrayAdapter = new ArrayAdapter<Color>(
                this.getActivity(),
                android.R.layout.simple_spinner_item,
                colorArray
        );

        this.raceSpinner.setAdapter(raceArrayAdapter);
        this.colorSpinner.setAdapter(colorArrayAdapter);

        view.findViewById(R.id.ad_create_validate).setOnClickListener(this);
    }


    private void saveData() {
        Ad ad = new Ad();
        ad.setTitle(this.titleText.getText().toString());
        ad.setContent(this.contentText.getText().toString());
        ad.setValidated(true);

        Animal animal = new Animal();
        animal.setAge(this.ageText.getText().toString());
        animal.setNature(this.nature.getText().toString());
        animal.setIdentification(this.identificationText.getText().toString());
        animal.setMotherIdentification(this.motheridentificationText.getText().toString());
        animal.setSex(this.femaleButton.isChecked() ? 1 : 0);
        animal.setVaccined(this.isVaccinedBox.isChecked());
        animal.setPuced(this.isTatooedBox.isChecked());
        animal.setTatoo(this.isTatooedBox.isChecked());
        animal.setSterilized(this.isSterilizedBox.isChecked());
        animal.setDewormed(this.isDewormedBox.isChecked());

        animal.setRace(((Race)this.raceSpinner.getSelectedItem()).getIntValue());
        animal.setColor(((Color)this.colorSpinner.getSelectedItem()).getIntValue());

        AnimalSQLiteAdapter animalSQLiteAdapter = new AnimalSQLiteAdapter(this.getActivity());
        animal.setId((int) animalSQLiteAdapter.insert(animal));

        ad.setAnimal(animal);

        AdSQLiteAdapter adSQLiteAdapter = new AdSQLiteAdapter(this.getActivity());
        adSQLiteAdapter.insert(ad);

        AdWebServiceAdapter adWebServiceAdapter = new AdWebServiceAdapter(this.getActivity());

        adWebServiceAdapter.post(ad);

        this.getActivity().finish();
    }

    @Override
    public void onClick(View v) {
        this.saveData();
    }
}
