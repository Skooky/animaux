package com.findmehome.findmehome.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.findmehome.findmehome.entity.base.EntityBase;

import java.io.Serializable;

/**
 * Created by maxime on 13/06/2017.
 */

public class Animal extends EntityBase implements Serializable, Parcelable {
    private String age;
    private boolean isVaccined;
    private boolean isPuced;
    private boolean isSterilized;
    private boolean isDewormed;
    private boolean isTatoo;
    private int nbWorm;
    private String identification;
    private String motherIdentification;
    private int sex;
    private int color;
    private int race;
    private int health;
    private String nature;
    private int child;

    public Animal() {
    }

    protected Animal(Parcel in) {
        super(in);

        age = in.readString();
        isVaccined = in.readByte() != 0;
        isPuced = in.readByte() != 0;
        isSterilized = in.readByte() != 0;
        isDewormed = in.readByte() != 0;
        isTatoo = in.readByte() != 0;
        nbWorm = in.readInt();
        identification = in.readString();
        motherIdentification = in.readString();
        sex = in.readInt();
        color = in.readInt();
        race = in.readInt();
        health = in.readInt();
        nature = in.readString();
        child = in.readInt();
    }

    public static final Creator<Animal> CREATOR = new Creator<Animal>() {
        @Override
        public Animal createFromParcel(Parcel in) {
            return new Animal(in);
        }

        @Override
        public Animal[] newArray(int size) {
            return new Animal[size];
        }
    };

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public boolean isVaccined() {
        return isVaccined;
    }

    public void setVaccined(boolean vaccined) {
        isVaccined = vaccined;
    }

    public boolean isPuced() {
        return isPuced;
    }

    public void setPuced(boolean puced) {
        isPuced = puced;
    }

    public boolean isSterilized() {
        return isSterilized;
    }

    public void setSterilized(boolean sterilized) {
        isSterilized = sterilized;
    }

    public boolean isDewormed() {
        return isDewormed;
    }

    public void setDewormed(boolean dewormed) {
        isDewormed = dewormed;
    }

    public boolean isTatoo() {
        return isTatoo;
    }

    public void setTatoo(boolean tatoo) {
        isTatoo = tatoo;
    }

    public int getNbWorn() {
        return nbWorm;
    }

    public void setNbWorn(int nbWorm) {
        this.nbWorm = nbWorm;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getMotherIdentification() {
        return motherIdentification;
    }

    public void setMotherIdentification(String motherIdentification) {
        this.motherIdentification = motherIdentification;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getRace() {
        return race;
    }

    public void setRace(int race) {
        this.race = race;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public int getChild() {
        return child;
    }

    public void setChild(int child) {
        this.child = child;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(age);
        parcel.writeByte((byte) (isVaccined ? 1 : 0));
        parcel.writeByte((byte) (isPuced ? 1 : 0));
        parcel.writeByte((byte) (isSterilized ? 1 : 0));
        parcel.writeByte((byte) (isDewormed ? 1 : 0));
        parcel.writeByte((byte) (isTatoo ? 1 : 0));
        parcel.writeInt(nbWorm);
        parcel.writeString(identification);
        parcel.writeString(motherIdentification);
        parcel.writeInt(sex);
        parcel.writeInt(color);
        parcel.writeInt(race);
        parcel.writeInt(health);
        parcel.writeString(nature);
        parcel.writeInt(child);
    }
}
