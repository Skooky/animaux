package com.findmehome.findmehome.view.Login;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.findmehome.findmehome.R;
import com.findmehome.findmehome.data.webservice.UserWebServiceAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {

    private EditText usernameText;
    private EditText passwordText;
    private Button loginButton;


    public LoginFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        this.initializeView(view);

        return view;
    }

    /**
     * Initialize view.
     * @param view View
     */
    private void initializeView(View view) {
        this.usernameText = (EditText) view.findViewById(R.id.login_username);
        this.passwordText = (EditText) view.findViewById(R.id.login_password);

        this.loginButton = (Button) view.findViewById(R.id.login_button);

        this.loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (!TextUtils.isEmpty(this.usernameText.getText().toString())
                && !TextUtils.isEmpty(this.passwordText.getText().toString())) {

            final String username = this.usernameText.getText().toString();
            final String password = this.passwordText.getText().toString();

            new AsyncTask<Void, Boolean, Boolean>() {
                ProgressDialog progressDialog = new ProgressDialog(LoginFragment.this.getActivity());

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

                    progressDialog.setTitle("Connexion...");
                    progressDialog.setMessage("Connexion en cours ...");
                    progressDialog.show();
                }

                @Override
                protected Boolean doInBackground(Void... params) {
                    boolean result = true;

                    try {
                        new UserWebServiceAdapter(LoginFragment.this.getActivity()).authentication(username, password);

                    } catch (Exception e) {
                        result = false;
                    }

                    return result;
                }

                @Override
                protected void onPostExecute(Boolean aBoolean) {
                    super.onPostExecute(aBoolean);

                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    if (aBoolean) {
                        LoginFragment.this.getActivity().finish();
                    } else {
                        Toast.makeText(
                                LoginFragment.this.getActivity(),
                                "Une erreur s'est produite",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                }

            }.execute();
        } else {
            Toast.makeText(
                    this.getActivity(),
                    "Merci de remplir les champs",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }
}
