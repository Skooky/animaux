package com.findmehome.findmehome.data.contract;

import android.provider.BaseColumns;

import com.findmehome.findmehome.data.contract.base.ContractBase;

/**
 * Created by maxime on 13/06/2017.
 */

public class AdContract extends ContractBase {
    /**
     * Name of the table.
     */
    public static final String TABLE_NAME = "Ad";

    /**
     * Name of fields DB.
     */
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_CONTENT = "content";
    public static final String FIELD_PUBLISHED = "published";
    public static final String FIELD_ISVALIDATED = "is_validated";
    public static final String FIELD_USER = "user_id";
    public static final String FIELD_ANIMAL = "animal_id";

    /**
     * Columns.
     */
    public static final String[] COLUMNS = {
            AdContract.TABLE_NAME + "." + AdContract.FIELD_ID + " AS " + BaseColumns._ID,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_SERVERID,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_TITLE,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_CONTENT,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_PUBLISHED,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_ISVALIDATED,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_CREATED_DATE,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_UPDATED_DATE,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_SYNC_DATE,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_USER,
            AdContract.TABLE_NAME + "." + AdContract.FIELD_ANIMAL,
    };

    @Override
    public String createTable() {
        return "CREATE TABLE " + AdContract.TABLE_NAME + " ( " +
                AdContract.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                AdContract.FIELD_SERVERID + " INTEGER, " +
                AdContract.FIELD_CREATED_DATE + " DATETIME, " +
                AdContract.FIELD_UPDATED_DATE + " DATETIME, " +
                AdContract.FIELD_SYNC_DATE + " DATETIME, " +
                AdContract.FIELD_TITLE + " TEXT NOT NULL, " +
                AdContract.FIELD_CONTENT + " TEXT NOT NULL, " +
                AdContract.FIELD_PUBLISHED + " DATETIME, " +
                AdContract.FIELD_ISVALIDATED + " INTEGER, " +
                AdContract.FIELD_USER + " INTEGER, " +
                AdContract.FIELD_ANIMAL + " INTEGER, " +
                "FOREIGN KEY (" + FIELD_USER + ") REFERENCES " +
                UserContract.TABLE_NAME + "(" + UserContract.FIELD_ID + ") " +
                "FOREIGN KEY (" + FIELD_ANIMAL + ") REFERENCES " +
                AnimalContract.TABLE_NAME + "(" + AnimalContract.FIELD_ID + ") " +
                ");";
    }
}
