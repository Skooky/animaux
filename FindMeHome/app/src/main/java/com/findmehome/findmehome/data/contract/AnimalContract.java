package com.findmehome.findmehome.data.contract;

import android.provider.BaseColumns;

import com.findmehome.findmehome.data.contract.base.ContractBase;

/**
 * Created by maxime on 13/06/2017.
 */

public class AnimalContract extends ContractBase {
    /**
     * Name of the table.
     */
    public static final String TABLE_NAME = "Animal";

    /**
     * Name of fields DB.
     */
    public static final String FIELD_AGE = "age";
    public static final String FIELD_ISVACCINED = "is_vaccined";
    public static final String FIELD_ISPUCED = "is_puced";
    public static final String FIELD_ISSTERILIZED = "is_sterilized";
    public static final String FIELD_ISDEWORMED = "is_dewormed";
    public static final String FIELD_ISTATOO = "is_tatoo";
    public static final String FIELD_NBWORN = "nb_worn";
    public static final String FIELD_IDENTIFICATION = "identification";
    public static final String FIELD_MOTHERIDENTIFICATION = "mother_identification";
    public static final String FIELD_SEX = "sex";
    public static final String FIELD_COLOR = "color";
    public static final String FIELD_RACE = "race";
    public static final String FIELD_HEALTH = "health";
    public static final String FIELD_NATURE= "nature";
    public static final String FIELD_CHILD = "child";

    /**
     * Columns.
     */
    public static final String[] COLUMNS = {
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_ID + " AS " + BaseColumns._ID,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_SERVERID,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_AGE,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_ISVACCINED,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_ISPUCED,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_ISSTERILIZED,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_ISDEWORMED,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_ISTATOO,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_NBWORN,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_IDENTIFICATION,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_MOTHERIDENTIFICATION,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_SEX,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_COLOR,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_RACE,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_HEALTH,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_NATURE,
            AnimalContract.TABLE_NAME + "." + AnimalContract.FIELD_CHILD,
    };

    @Override
    public String createTable() {
        return "CREATE TABLE " + AnimalContract.TABLE_NAME + " ( " +
                AnimalContract.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                AnimalContract.FIELD_SERVERID + " INTEGER, " +
                AnimalContract.FIELD_CREATED_DATE + " DATETIME, " +
                AnimalContract.FIELD_UPDATED_DATE + " DATETIME, " +
                AnimalContract.FIELD_SYNC_DATE + " DATETIME, " +
                AnimalContract.FIELD_AGE + " INTEGER, " +
                AnimalContract.FIELD_ISVACCINED + " INTEGER, " +
                AnimalContract.FIELD_ISPUCED + " INTEGER, " +
                AnimalContract.FIELD_ISSTERILIZED + " INTEGER, " +
                AnimalContract.FIELD_ISDEWORMED + " INTEGER, " +
                AnimalContract.FIELD_ISTATOO + " INTEGER, " +
                AnimalContract.FIELD_NBWORN + " INTEGER, " +
                AnimalContract.FIELD_IDENTIFICATION + " TEXT, " +
                AnimalContract.FIELD_MOTHERIDENTIFICATION + " TEXT, " +
                AnimalContract.FIELD_SEX + " INTEGER, " +
                AnimalContract.FIELD_COLOR + " INTEGER, " +
                AnimalContract.FIELD_RACE + " INTEGER, " +
                AnimalContract.FIELD_HEALTH + " INTEGER, " +
                AnimalContract.FIELD_NATURE + " TEXT, " +
                AnimalContract.FIELD_CHILD + " INTEGER " +
                ");";
    }
}
