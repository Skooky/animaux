package com.findmehome.findmehome.data.base.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.findmehome.findmehome.data.base.adapter.base.SQLiteAdapterBase;
import com.findmehome.findmehome.data.contract.AddressContract;
import com.findmehome.findmehome.entity.Address;
import com.findmehome.findmehome.utils.DateUtils;
import com.findmehome.findmehome.utils.PreferencesUtils;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by maxime on 13/06/2017.
 */

public class AddressSQLiteAdapter extends SQLiteAdapterBase<Address> {

    /**
     * Constructor.
     *
     * @param context Le context
     */
    public AddressSQLiteAdapter(Context context) {
        super(context);
    }

    @Override
    public long insert(Address model) {
        this.open();

        long result = this.db.insert(AddressContract.TABLE_NAME, null, this.itemToContentValues(model));

        this.close();

        return result;
    }

    @Override
    public void update(Address model) {
        this.open();

        model.setUpdatedDate(new DateTime());

        this.db.update(
                AddressContract.TABLE_NAME,
                this.itemToContentValues(model),
                AddressContract.FIELD_ID + " = ?",
                new String[] {String.valueOf(model.getId())});

        this.close();
    }

    @Override
    public long insertOrUpdate(Address model) {
        Address dbAddress = this.getWithServerId(model.getServerId());
        long result = -1;

        if (dbAddress != null) {
            model.setId(dbAddress.getId());
            model.setSyncDate(PreferencesUtils.getSyncDate(this.getContext()));

            this.update(model);
        } else {
            result = this.insert(model);
        }

        return result;
    }

    @Override
    public void insertOrUpdate(ArrayList<Address> models) {
        for (Address Address : models) {
            this.insertOrUpdate(Address);
        }
    }

    @Override
    public void delete(Address model) {
        this.deleteWithId(model.getId());
    }

    @Override
    public void deleteWithId(int modelId) {
        this.open();

        this.db.delete(AddressContract.TABLE_NAME, AddressContract.FIELD_ID + " = ?", new String[] {String.valueOf(modelId)});

        this.close();
    }

    @Override
    public Address get(Address model) {
        return this.getWithId(model.getId());
    }

    @Override
    public Address getWithId(int modelId) {
        this.open();
        Address result = null;

        Cursor cursor = this.query(
                AddressContract.FIELD_ID + " = ?",
                new String[] {String.valueOf(modelId)},
                null,
                null,
                null,
                null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            result = this.cursorToItem(cursor);

            cursor.close();
        }

        this.close();

        return result;
    }

    @Override
    public Address getWithServerId(int modelServerId) {
        this.open();
        Address result = null;

        Cursor cursor = this.query(
                AddressContract.FIELD_SERVERID + " = ?",
                new String[] {String.valueOf(modelServerId)},
                null,
                null,
                null,
                null);

        if (cursor.getCount() != 0) {
            cursor.moveToFirst();

            result = this.cursorToItem(cursor);

            cursor.close();
        }

        this.close();

        return result;
    }

    @Override
    public ArrayList<Address> getAll() {
        this.open();

        ArrayList<Address> result;

        Cursor cursor = this.query(null, null, null, null, null, null);

        result = this.cursorToItems(cursor);

        this.close();
        return result;
    }

    @Override
    public ContentValues itemToContentValues(Address item) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(AddressContract.FIELD_SERVERID, item.getServerId());

        contentValues.put(AddressContract.FIELD_STREET, item.getStreet());
        contentValues.put(AddressContract.FIELD_CITY, item.getCity());
        contentValues.put(AddressContract.FIELD_ZIPCODE, item.getZipcode());

        if (item.getCreatedDate() == null) {
            item.setCreatedDate(new DateTime());
        }

        contentValues.put(AddressContract.FIELD_CREATED_DATE,
                DateUtils.formatDateTimeToString(item.getCreatedDate()));

        if (item.getUpdatedDate() != null) {
            contentValues.put(AddressContract.FIELD_UPDATED_DATE,
                    DateUtils.formatDateTimeToString(item.getUpdatedDate()));
        }

        if (item.getSyncDate() != null) {
            contentValues.put(AddressContract.FIELD_SYNC_DATE,
                    DateUtils.formatDateTimeToString(item.getSyncDate()));
        }

        return contentValues;
    }

    @Override
    public Address cursorToItem(Cursor cursor) {
        Address result = new Address();
        int column;

        if ((column = cursor.getColumnIndex(BaseColumns._ID)) > -1) {
            result.setId(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AddressContract.FIELD_SERVERID)) > -1) {
            result.setServerId(cursor.getInt(column));
        }

        if ((column = cursor.getColumnIndex(AddressContract.FIELD_STREET)) > -1) {
            result.setStreet(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(AddressContract.FIELD_CITY)) > -1) {
            result.setCity(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(AddressContract.FIELD_ZIPCODE)) > -1) {
            result.setZipcode(cursor.getString(column));
        }

        if ((column = cursor.getColumnIndex(AddressContract.FIELD_CREATED_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setCreatedDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        if ((column = cursor.getColumnIndex(AddressContract.FIELD_UPDATED_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setUpdatedDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        if ((column = cursor.getColumnIndex(AddressContract.FIELD_SYNC_DATE)) > -1) {
            if (cursor.getString(column) != null) {
                result.setSyncDate(DateUtils.formatStringToDateTime(cursor.getString(column)));
            }
        }

        return result;
    }

    @Override
    public ArrayList<Address> cursorToItems(Cursor cursor) {
        ArrayList<Address> result = new ArrayList<>();

        if (cursor.getCount() != 0) {
            while (cursor.moveToNext()) {
                result.add(this.cursorToItem(cursor));
            }

            cursor.close();
        }

        return result;
    }

    @Override
    public Cursor query(
            String whereClause,
            String[] whereArgs,
            String groupBy,
            String having,
            String orderBy,
            String limit) {
        return this.db.query(
                AddressContract.TABLE_NAME,
                AddressContract.COLUMNS,
                whereClause,
                whereArgs,
                groupBy,
                having,
                orderBy,
                limit);
    }

    @Override
    public void deleteUnused() {
        this.db.delete(
                AddressContract.TABLE_NAME,
                AddressContract.FIELD_SYNC_DATE + " <> ?",
                new String[] { String.valueOf(PreferencesUtils.getSyncDate(this.getContext()).getMillis())}
        );
    }
}
