package com.findmehome.findmehome.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.findmehome.findmehome.entity.base.EntityBase;

import java.io.Serializable;

/**
 * Created by maxime on 13/06/2017.
 */

public class Address extends EntityBase implements Serializable, Parcelable {
    /** Street. */
    private String street;
    /** City. */
    private String city;
    /** ZipCode. */
    private String zipcode;

    /**
     * Constructor.
     */
    public Address() {
    }

    /**
     * Get street.
     * @return String
     */
    public String getStreet() {
        return street;
    }

    /**
     * Set street.
     * @param street {@link String}
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Get city.
     * @return city
     */
    public String getCity() {
        return city;
    }

    /**
     * Set city.
     * @param city {@link String}
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Get zipcode.
     * @return String
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * Set zipcode.
     * @param zipcode {@link String}
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     * Constructor.
     * @param in {@link Parcel}
     */
    protected Address(Parcel in) {
        super(in);

        this.street = in.readString();
        this.city = in.readString();
        this.zipcode = in.readString();
    }

    /**
     * Parcel creator.
     */
    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeString(street);
        parcel.writeString(city);
        parcel.writeString(zipcode);
    }


}
