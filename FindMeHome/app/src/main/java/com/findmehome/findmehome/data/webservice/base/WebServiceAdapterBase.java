package com.findmehome.findmehome.data.webservice.base;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sandra on 13/06/17.
 */

public abstract class WebServiceAdapterBase<T> {
    /** Context. */
    private Context context;

    /**
     * Constructor.
     * @param context {@link Context}
     */
    public WebServiceAdapterBase(Context context) {
        this.context = context;
    }

    /**
     * JSON to object.
     * @param jsonObject {@link JSONObject}
     * @param isData boolean
     * @return T entity
     */
    public abstract T jsonToObject(JSONObject jsonObject, boolean isData);

    /**
     * {@link JSONArray} to object.
     * @param jsonArray {@link JSONArray}
     * @return ArrayList<T> where T = entity
     */
    public abstract ArrayList<T> jsonArrayToObject(JSONArray jsonArray);

    /**
     * Object to {@link JSONObject}.
     * @param object T = Entity
     * @return JSONObject
     */
    public abstract JSONObject objectToJson(T object);

    /**
     * Object to {@link JSONArray}.
     * @param objects {@link ArrayList} entity
     * @return JSONArray
     */
    public abstract JSONArray objectToJsonArray(ArrayList<T> objects);

    /**
     * Get entity by serverId.
     * @param serverId int
     * @return T entity
     */
    public abstract T get(int serverId);

    /**
     * Get all entities.
     * @return ArrayList of entities
     */
    public abstract ArrayList<T> getAll();

    /**
     * POST request.
     * @param model entity
     */
    public abstract void post(T model);

    /**
     * PUT request.
     * @param model entity
     */
    public abstract void put(T model);

    /**
     *
     * @param serverId int
     */
    public abstract void delete(int serverId);

    /**
     * Get context.
     * @return Context
     */
    public Context getContext() {
        return context;
    }
}
