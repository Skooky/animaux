package com.findmehome.findmehome.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.joda.time.DateTime;



/**
 * This class allow to put data into the phone without use db.
 * @author sbristiel
 */
public class PreferencesUtils {
    /** Pref name. */
    private static final String PREFERENCE = "PreferenceFindMeHome";

    /** Sync variable. */
    private static final String PREFERENCE_SYNCDATE = "SyncDate";
    /** Token variable. */
    private static final String PREFERENCE_TOKEN = "token";

    /**
     * Get last sync date.
     * @param context {@link Context}
     * @return DateTime syncDate
     */
    public static DateTime getSyncDate(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE, 0);

        long time = preferences.getLong(PREFERENCE_SYNCDATE, 0);

        DateTime result = null;

        if (time != 0) {
            result = new DateTime(time);
        }

        return result;
    }

    /**
     * Set syncDate as NOW.
     * @param context {@link Context}
     */
    public static void setSyncDate(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putLong(PREFERENCE_SYNCDATE, new DateTime().getMillis());

        editor.commit();
    }

    /**
     * Set token.
     * @param context {@link Context}
     * @param token String
     */
    public static void setToken(Context context, String token) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PREFERENCE_TOKEN, token);

        editor.commit();
    }

    /**
     * Get the token.
     * @param context {@link Context}
     * @return String token
     */
    public static String getToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCE, 0);

        String token = preferences.getString(PREFERENCE_TOKEN, "");

        return token;
    }
}
