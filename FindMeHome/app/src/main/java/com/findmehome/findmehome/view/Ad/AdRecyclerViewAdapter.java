package com.findmehome.findmehome.view.Ad;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.findmehome.findmehome.R;
import com.findmehome.findmehome.data.base.adapter.AdSQLiteAdapter;
import com.findmehome.findmehome.entity.Ad;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Ad} and makes a call to the
 * specified {@link AdListFragment.OnListFragmentInteractionListener}.
 */
public class AdRecyclerViewAdapter extends RecyclerView.Adapter<AdRecyclerViewAdapter.ViewHolder> {
    public CursorAdapter cursorAdapter;
    private AdSQLiteAdapter adSQLiteAdapter;
    private Context context;

    /**
     * Constructeur.
     * @param context {@link Context}
     * @param cursor {@link Cursor} le cursor de données à mettre dans le recycler view
     * @param listener permet de savoir quand un élément est cliqué
     */
    public AdRecyclerViewAdapter(
            Context context,
            Cursor cursor,
            final AdListFragment.OnListFragmentInteractionListener listener) {
        this.context = context;
        this.adSQLiteAdapter = new AdSQLiteAdapter(this.context);

        this.cursorAdapter = new CursorAdapter(this.context, cursor, 0) {
            private TextView titleView;
            private TextView contentView;

            @Override
            public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
                return LayoutInflater.from(context).inflate(R.layout.row_ad, viewGroup, false);
            }

            @Override
            public void bindView(final View view, Context context, Cursor cursor) {
                Ad item = adSQLiteAdapter.cursorToItem(cursor);

                this.titleView = (TextView) view.findViewById(R.id.row_ad_title);
                this.contentView = (TextView) view.findViewById(R.id.row_ad_content);

                this.titleView.setText(item.getTitle());
                this.contentView.setText(item.getContent());

                view.setTag(item.getId());

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != listener) {
                            Ad ad = new Ad();
                            ad.setId((Integer) v.getTag());
                            listener.onListFragmentInteraction(ad);
                        }
                    }
                });
            }
        };
    }

    /**
     * Reload the view with new data.
     * @param cursor {@link Cursor}
     */
    public void reloadCursor(Cursor cursor) {
        this.cursorAdapter.swapCursor(cursor);
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = this.cursorAdapter.newView(this.context, this.cursorAdapter.getCursor(), parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        this.cursorAdapter.getCursor().moveToPosition(position);
        this.cursorAdapter.bindView(holder.itemView, this.context, this.cursorAdapter.getCursor());
    }

    @Override
    public int getItemCount() {
        return this.cursorAdapter.getCount();
    }

    /**
     * Vue qui correspond à chaque row de la recycler view.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View view;

        public ViewHolder(View view) {
            super(view);

            this.view = view;
        }
    }
}
