package com.findmehome.findmehome.data.base.adapter.base;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.findmehome.findmehome.data.base.FindMeHomeSQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by maxime on 13/06/2017.
 */

public abstract class SQLiteAdapterBase<T> {

    /**
     * DB of the application.
     */
    protected SQLiteDatabase db;
    /**
     * Helper of the l'application.
     */
    private FindMeHomeSQLiteOpenHelper openHelper;
    /**
     * Context of the l'application.
     */
    protected Context context;

    /**
     * Adapter constructor's.
     *
     * @param context Le context
     */
    public SQLiteAdapterBase(Context context) {
        this.context = context;
        this.openHelper = FindMeHomeSQLiteOpenHelper.getInstance(context);
    }

    /**
     * Open the database (Must be call before every call)
     *
     * @return SQLiteDatabase db
     */
    protected SQLiteDatabase open() {
        this.db = this.openHelper.getWritableDatabase();

        return this.db;
    }

    /**
     * Close the database. (Must be call after every call)
     */
    protected void close() {
        this.db.close();
    }

    /**
     * Insert into db.
     *
     * @param model entity to add
     * @return long entity id
     */
    public abstract long insert(T model);

    /**
     * Update into db.
     *
     * @param model entity to update
     */
    public abstract void update(T model);

    /**
     * Insert or update an entity.
     *
     * @param model entity to add or update
     * @return long entity id
     */
    public abstract long insertOrUpdate(T model);

    /**
     * Insert or update entities.
     *
     * @param models List of entities
     */
    public abstract void insertOrUpdate(ArrayList<T> models);

    /**
     * Delete the entity.
     *
     * @param model entity to delete
     */
    public abstract void delete(T model);

    /**
     * Delete the entity by id.
     *
     * @param modelId entity id
     */
    public abstract void deleteWithId(int modelId);

    /**
     * Get entity in db.
     *
     * @param model entity to get.
     */
    public abstract T get(T model);

    /**
     * Get entity with id.
     *
     * @param modelId entity id
     */
    public abstract T getWithId(int modelId);

    /**
     * Get entity with serverid.
     *
     * @param modelServerId entity serverId
     */
    public abstract T getWithServerId(int modelServerId);

    /**
     * Get all entities.
     */
    public abstract ArrayList<T> getAll();

    /**
     * Change the entity into contentvalues.
     *
     * @param item T
     * @return ContentValues
     */
    public abstract ContentValues itemToContentValues(T item);

    /**
     * Change the cursor into an entity.
     *
     * @param cursor {@link Cursor}
     * @return T l'objet
     */
    public abstract T cursorToItem(Cursor cursor);

    /**
     * Change the cursor into an array of entities.
     *
     * @param cursor {@link Cursor}
     * @return ArrayList<T>
     */
    public abstract ArrayList<T> cursorToItems(Cursor cursor);

    /**
     * Default query.
     *
     * @param whereClause WHERE, "Table.Id = ?"
     * @param whereArgs   WHERE args, "1"
     * @param groupBy     GROUP BY
     * @param having      HAVING
     * @param orderBy     ORDERBY
     * @param limit       LIMIT
     * @return Cursor cursor with the results
     */
    public abstract Cursor query(
            String whereClause,
            String[] whereArgs,
            String groupBy,
            String having,
            String orderBy,
            String limit);

    /**
     * Delete all the entities that doesn't exist anymore.
     */
    public abstract void deleteUnused();

    /**
     * Get the context.
     *
     * @return Context
     */
    public Context getContext() {
        return context;
    }
}
