package com.findmehome.findmehome.view.Ad;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.findmehome.findmehome.FindMeHomeApplication;
import com.findmehome.findmehome.R;
import com.findmehome.findmehome.data.base.adapter.AdSQLiteAdapter;
import com.findmehome.findmehome.data.webservice.AdWebServiceAdapter;
import com.findmehome.findmehome.entity.Ad;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class AdListFragment extends Fragment {

    private int columnCount = 1;
    private OnListFragmentInteractionListener listener;
    private AdSQLiteAdapter adSQLiteAdapter;
    private RecyclerView recyclerView;
    private AdRecyclerViewAdapter adRecyclerViewAdapter;
    private AdWebServiceAdapter adWebServiceAdapter;
    private String type = "Dons";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public AdListFragment() {
        this.adWebServiceAdapter = new AdWebServiceAdapter(FindMeHomeApplication.context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ad_list, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            this.type = bundle.getString("type");

        }

        this.adSQLiteAdapter = new AdSQLiteAdapter(this.getActivity());

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            this.recyclerView = (RecyclerView) view;
            if (columnCount <= 1) {
                this.recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                this.recyclerView.setLayoutManager(new GridLayoutManager(context, columnCount));
            }

            this.adRecyclerViewAdapter = new AdRecyclerViewAdapter(
                    this.getActivity(),
                    this.adSQLiteAdapter.getAllCursor(),
                    this.listener
            );

            this.recyclerView.setAdapter(this.adRecyclerViewAdapter);
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                AdListFragment.this.adWebServiceAdapter.getAll(AdListFragment.this.type);

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                AdListFragment.this.adRecyclerViewAdapter.reloadCursor(AdListFragment.this.adSQLiteAdapter.getAllCursor());
            }
        }.execute();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        this.adRecyclerViewAdapter.reloadCursor(this.adSQLiteAdapter.getAllCursor());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            listener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Ad item);
    }
}
