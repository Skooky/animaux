package com.findmehome.findmehome.data.webservice;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.toolbox.RequestFuture;
import com.findmehome.findmehome.data.contract.UserContract;
import com.findmehome.findmehome.data.webservice.base.RestClientBase;
import com.findmehome.findmehome.data.webservice.base.WebServiceAdapterBase;
import com.findmehome.findmehome.entity.User;
import com.findmehome.findmehome.utils.PreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by sandra on 13/06/17.
 */

public class UserWebServiceAdapter extends WebServiceAdapterBase<User> {

    /** Address for the entity. */
    private static final String USERS = "users";

    /** {@link AddressWebServiceAdapter}. */
    private AddressWebServiceAdapter addressWebServiceAdapter;

    /**
     * Constructor.
     * @param context {@link Context}
     */
    public UserWebServiceAdapter(Context context) {
        super(context);

        this.addressWebServiceAdapter = new AddressWebServiceAdapter(context);
    }

    @Override
    public User jsonToObject(JSONObject jsonObject, boolean isData) {
        User result = new User();

        try {
            JSONObject object = jsonObject;

            if (isData) {
                object = (JSONObject) jsonObject.get("data");
            }

            result.setServerId(object.getInt("id"));
            result.setEmail(object.getString(UserContract.FIELD_EMAIL));
            result.setFirstName(object.getString(UserContract.FIELD_FIRSTNAME));
            result.setLastName(object.getString(UserContract.FIELD_LASTNAME));
            result.setPhone(object.getString(UserContract.FIELD_PHONE));
            result.setSiren(object.getString(UserContract.FIELD_SIREN));
            result.setPro(object.getBoolean(UserContract.FIELD_ISPRO));
            result.setSocietyName(object.getString(UserContract.FIELD_SOCIETYNAME));
            result.setUserName(object.getString(UserContract.FIELD_USERNAME));
            result.setAddress(this.addressWebServiceAdapter.jsonToObject(
                    jsonObject.getJSONObject("address"), false));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public ArrayList<User> jsonArrayToObject(JSONArray jsonArray) {
        ArrayList<User> result = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                result.add(this.jsonToObject((JSONObject) jsonArray.get(i), false));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    @Override
    public JSONObject objectToJson(User object) {
        JSONObject result = new JSONObject();

        try {
            result.put(UserContract.FIELD_USERNAME, object.getUserName());
            result.put(UserContract.FIELD_FIRSTNAME, object.getFirstName());
            result.put(UserContract.FIELD_LASTNAME, object.getLastName());
            result.put(UserContract.FIELD_EMAIL, object.getEmail());
            result.put(UserContract.FIELD_PHONE, object.getPhone());
            result.put(UserContract.FIELD_SIREN, object.getSiren());
            result.put(UserContract.FIELD_ISPRO, object.isPro());
            result.put(UserContract.FIELD_SOCIETYNAME, object.getSocietyName());
            result.put("address", this.addressWebServiceAdapter.objectToJson(object.getAddress()));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public JSONArray objectToJsonArray(ArrayList<User> objects) {
        JSONArray jsonArray = new JSONArray();

        for (User user : objects) {
            jsonArray.put(this.objectToJson(user));
        }

        return jsonArray;
    }

    @Override
    public User get(int serverId) {
        User result = null;

        try {
            JSONObject jsonObject = RestClientBase.getInstance(this.getContext())
                    .getJSONRequest(USERS+"/"+String.valueOf(serverId)).get(30, TimeUnit.SECONDS);

            result = this.jsonToObject(jsonObject, true);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Log.e(UserWebServiceAdapter.class.getName(), e.getMessage());
        }

        return result;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList<User> result = new ArrayList<>();

        try {
            JSONObject jsonObject = RestClientBase.getInstance(
                    this.getContext()).getJSONRequest(USERS).get(30, TimeUnit.SECONDS);

            result = this.jsonArrayToObject(jsonObject.getJSONArray("data"));

        } catch (InterruptedException | ExecutionException | TimeoutException | JSONException e) {
            Log.e(UserWebServiceAdapter.class.getName(), e.getMessage());
        }

        return result;
    }

    @Override
    public void post(User model) {
        JSONObject postObject = this.objectToJson(model);

        RestClientBase.getInstance(
                this.getContext()).postJSONRequest(USERS + "/register", postObject);
    }

    public void post(User model, String password) {
        JSONObject postObject = this.objectToJson(model);
        try {
            postObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject response = RestClientBase.getInstance(this.getContext()).postJSONRequest(USERS + "/register", postObject).get(30, TimeUnit.SECONDS);

            JSONObject data = response.getJSONObject("data");
            String token = data.getString("token");

            PreferencesUtils.setToken(this.getContext(), token);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void put(User model) {
        JSONObject putObject = this.objectToJson(model);

        RestClientBase.getInstance(
                this.getContext()).putJSONRequest(USERS+"/"+model.getServerId(), putObject);
    }

    @Override
    public void delete(int serverId) {
        RestClientBase.getInstance(
                this.getContext()).deleteJSONRequest(USERS+"/"+String.valueOf(serverId));

    }

    public void authentication(String login, String password) throws Exception {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("username", login);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject response = RestClientBase.getInstance(this.getContext()).postJSONRequest(USERS + "/token", jsonObject).get(30, TimeUnit.SECONDS);

        try {
            JSONObject data = response.getJSONObject("data");
            String token = data.getString("token");

            PreferencesUtils.setToken(this.getContext(), token);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
